<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'purepitwall_v1');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Yah_>Vf!]/Z]7E)6>$&Z+5c+|;y0*lY8N6Z@Y) 2c|PH[|!YIR/V8;-t(p.GEtO!');
define('SECURE_AUTH_KEY',  '#=(iP2pPSa>AL,Xf%147w0WL70Gm) T#4T{WYH)V%2NMNS^yk]@>8;48^x33iBx`');
define('LOGGED_IN_KEY',    'As5`ZX$;a5bT+;3%k%xVxnhJG4^/UmjMA(7K4xaUVy4 Gl38 bHtY]C,DO_hmT<E');
define('NONCE_KEY',        ')[_4Z$>08f kyXxS<lw9ee1Z3B3o,K<rhn>8q_lMcu-#x+;AJ,~:bK]6OpQ^|.:J');
define('AUTH_SALT',        '%&217%%OH(J|HFT2]K&9>;WtC,oSDJPwe[W%d)T~A#nX=*gFy7[=5UtOl#)Jb=K-');
define('SECURE_AUTH_SALT', 'J!dax `OCG[U5Q@Bd<V%q6:&?&6RDhg`6*!B&WPjk}A`Z6bXd&^Trq3O{/K9IPCP');
define('LOGGED_IN_SALT',   '_.f2uZ%>^F~G<MIPcY2 {+2Ypv~Ppf<c~Xz$m?3=g+Qdr<o!ZN;fJZ6T|-/2vSm[');
define('NONCE_SALT',       'J%AWS rF<$|-e6<-B5<*$BC>IR=04on%+*;[JWG>Rg3VSS83~4Dc([ 0%t`Wf(py');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/*define('WP_HOME','https://purepitwall.local9');
define('WP_SITEURL','https://purepitwall.local9');*/
