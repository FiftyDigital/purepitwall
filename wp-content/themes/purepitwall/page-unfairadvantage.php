<?php get_header(); ?>
<section class="unfairAdvantage" role="main">

    <div class="ua_header">
        <?php /* Start the Pods Loop */
            // Pulling PODS Loop instead of WP Loop
            $mypod = pods('home_hero_video');
            $params = array(
                'limit' => 1,
                'orderby' => 'date DESC'
                );
            $mypod->find($params);
        ?>
        <?php while ( $mypod->fetch() ) : ?>
            <?php
                $videoFile = wp_get_attachment_url($mypod->field('video_file.ID'));
                $videoPoster = wp_get_attachment_url($mypod->field('video_fallback_image.ID'));
            ?>
            <div class="videoBg js-bgImg" style="background-image: url('<?php echo $videoPoster ?>')">
                <video class="bg-video" data-url="<?php echo $videoFile;?>" autoplay playsinline loop muted></video>
            </div>
        <?php endwhile; ?>
        <div class="videoBgOverlay"></div>
    </div>

    <div class="ua_module ua_products is-parallax">
        <div class="container-fluid">
            <div class="ua_products_title">
                <h3>Pure Storage are giving MERCEDES-AMG PETRONAS MOTORSPORT an</h3>
                <h1>#UNFAIR <span class="secondWord">ADVANTAGE</span>
            </div>
            <div class="productsContent">
                <div class="line1">
                    <h4>Each car is fitted<br class="hidden-md hidden-lg hidden-wd"> with more than<h4>
                </div>
                <div class="line2 stats-counter is-parallax" data-number="200">0</div>
                <div class="line3">
                    <h4>Physical Data <br class="hidden-md hidden-lg hidden-wd">Sensors</h4>
                </div>
            </div>
        </div>
        <div class="productsImg">
            <div class="sensor topLeft" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/ua_products_topLeft.png">
            </div>
            <div class="sensor topRight" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/ua_products_topRight.png">
            </div>
            <div class="sensor bottomLeft" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/ua_products_bottomLeft.png">
            </div>
            <div class="sensor bottomRight" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/ua_products_bottomRight.png">
            </div>
        </div>
    </div>
    <div class="ua_module ua_storage is-parallax">
        <div class="bigHolder">
            <div class="smallBar">
                <div class="barContent">
                    <span class="capture">Which capture at least</span><h2>400GB</h2>
                    <span class="legend">per race</span>
                </div>
            </div>
            <div class="bigBar">
                <div class="barContent">
                    <h1>9TB</h1>
                    <span class="legend">per season</span>
                </div>
            </div>
        </div>
    </div>
    <div class="ua_module ua_reduction is-parallax">
        <div class="container-fluid">
            <h4>The small footprint of the Pure Storage<br>array has meant a</h4>
            <h1><span class="stats-counter is-parallax" data-number="68">0</span>% <br class="hidden-sm hidden-md hidden-lg hidden-wd">reduction</h1>
            <h4>In Data-Center Rack Space</h4>

            <div class="graphHolder">
                <div class="graphBg">
                    <img src="<?php echo get_template_directory_uri();?>/assets/img/ua_reduction_A.svg">
                </div>
                <div class="graphLine">
                    <img src="<?php echo get_template_directory_uri();?>/assets/img/ua_reduction_B.svg">
                </div>
                <div class="mNum mNum25">
                    25%
                </div>
                <div class="mNum mNum50">
                    50%
                </div>
                <div class="mNum mNum68">
                    68%
                </div>
            </div>

            <h4>Saving</h4>
            <h1>£<span class="stats-counter is-parallax" data-number="100000">0</span> <br class="hidden-sm hidden-md hidden-lg hidden-wd">a year</h1>
            <h4>in operating costs.</h4>

        </div>
    </div>

    <div class="ua_module ua_hours is-parallax">
        <div class="stopwatchHolder">
            <div class="stopwatch watchTopLeft" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/ua_stopwatch02.png">
            </div>
            <div class="stopwatch watchTopRight" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/ua_stopwatch03.png">
            </div>
            <div class="stopwatch watchBottomLeft" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/ua_stopwatch01.png">
            </div>
            <div class="stopwatch watchBottomRight" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/ua_stopwatch01.png">
            </div>
        </div>
        <div class="container-fluid">
            <div class="hoursCopyTop">
                <h4>CFD Application require massive<br> processing power and can take up to</h4>
                <h1><span class="stats-counter is-parallax" data-number="20">0</span> hours <br class="hidden-sm hidden-md hidden-lg hidden-wd">to run.</h1>
            </div>
            <div class="hoursCopyBottom">
                <h4>Testing has shown that moving these<br> applications onto Pure Storage has</h4>
                <h1>Reduced processing<br>time by up to <span class="stats-counter is-parallax" data-number="15">0</span>%</h1>
            </div>
        </div>
    </div>

    <div class="ua_module ua_flasharray">
        <div class="box boxLeft" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/ua_flasharrayM.png">
            <div class="copyLeft">
                <h2>FLASHARRAY//M JUST WORKS.</h2>
                <p>For two years, //M has delivered <span class="orangeText">99.9999%</span> availability- inclusive of maintenance, failures, and now generational upgrades. That means your data is always-on, always-fast, and always-secure.</p>
            </div>
            <div class="ctaHolder">
                <a class="btn" href="https://www.purestorage.com/uk/products/flash-array-m.html" target="new">LEARN MORE</a>
            </div>
        </div>
        <div class="box boxRight" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/ua_flasharrayX.jpg">
            <div class="copyRight">
                <h2><span class="orangeText">INTRODUCING FLASHARRAY//X</span></h2>
                <p>Imagine putting all your workloads on just 10 flash modules – while getting the full power of your current Tier 1 array. //X has the performance density to consolidate all of your apps, and features microsecond response time and half a petabyte of effective capacity.</p>
            </div>
            <div class="ctaHolder">
                <a class="btn btn-white" href="https://www.purestorage.com/uk/products/flasharray-x.html" target="new">LEARN MORE</a>
            </div>
        </div>
        <div class="box boxRight" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/ua_flashblade.jpg">
            <div class="copyRight">
                <h2><span class="orangeText">INTRODUCING FLASHBLADE™</span></h2>
                <p>FlashBlade™ is the industry’s most advanced storage for unstructured data, consolidating complex data silos (like backup appliances and data lakes) to simplify infrastructure and accelerate tomorrow’s discoveries and insights.</p>
            </div>
            <div class="ctaHolder">
                <a class="btn btn-white" href="https://www.purestorage.com/uk/products/flashblade.html" target="new">LEARN MORE</a>
            </div>
        </div>
    </div>


    <div class="ua_module ua_slides">
        <div class="swiper-container">
            <div class="swiper-wrapper">

                <div class="slideHolder swiper-slide">
                    <div class="slidesBg" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/ua_slide3.jpg">
                    </div>
                    <div class="slidesCopy">
                        <h2>Data <span class="orangeText">streams</span> in from across the <span class="orangeText">ecosystem</span> in real-time, forming an <span class="orangeText">essential</span> pool of <span class="orangeText">intelligence.</span></h2>
                        <div class="ctaHolder">
                            <a class="btn" href="http://info.purestorage.com/secrets-of-Mercedes-AMG-Petronas-2017.html" target="new">READ THE TRANSFORMATION STORY</a>
                        </div>
                    </div>
                </div>

                <div class="slideHolder swiper-slide">
                    <div class="slidesBg" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/ua_slide2.jpg">
                    </div>
                    <div class="slidesCopy">
                        <h2><span class="orangeText">Data</span> is the <span class="orangeText">life force</span> of our operations and so many of our <span class="orangeText">employees</span> rely on it to be <span class="orangeText">effective</span> and <span class="orangeText">efficient.</span></h2>
                        <div class="ctaHolder">
                            <a class="btn" href="http://info.purestorage.com/secrets-of-Mercedes-AMG-Petronas-2017.html" target="new">READ THE TRANSFORMATION STORY</a>
                        </div>
                    </div>
                </div>

                <div class="slideHolder swiper-slide">
                    <div class="slidesBg" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/ua_slide1.jpg">
                    </div>
                    <div class="slidesCopy">
                        <h2>Combined, <span class="orangeText">big data</span> and <span class="orangeText">motorsport</span> are a marriage <span class="orangeText">made in heaven</span> since both are reliant on <span class="orangeText">velocity</span> to make a <span class="orangeText">winning</span> difference.</h2>
                        <div class="ctaHolder">
                            <a class="btn" href="http://info.purestorage.com/secrets-of-Mercedes-AMG-Petronas-2017.html" target="new">READ THE TRANSFORMATION STORY</a>
                        </div>
                    </div>
                </div>

            </div>
            <div class="controlsHolder">
                <div class="controls">
                    <div class="prevArrow">
                        <div class="arrow left"></div>
                    </div>
                    <div class="pagination"></div>
                    <div class="nextArrow">
                        <div class="arrow right"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="ua_module ua_videos">
        <div class="container-fluid">
            <h3>SEE HOW IT WORKS</h3>
        </div>

        <div class="videosHolder">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <!-- start with partnerships video -->
                        <div class="swiper-slide swiper-no-swiping">
                            <div class="videoWrapper">
                                <video controls poster="/wp-content/uploads/2017/02/2017_partnership.jpg" class="js-player">
                                    <source src="/wp-content/uploads/2018/02/2017Partnership.mp4" type="video/mp4">
                                </video>
                            </div>
                        </div>
                    <!-- end of partnership video -->

                    <?php /* Start the Pods Loop */
                        // Pulling PODS Loop instead of WP Loop
                        $mypod = pods('ua_videos');
                        $params = array(
                            'limit' => 6,
                             'orderby' => 'date DESC'
                            );
                        $mypod->find($params);
                    ?>

                    <?php while ( $mypod->fetch() ) : ?>
                        <?php
                            $video = $mypod->id();
                            $youtubeLink = $mypod->field('youtube_link');
                            $videoFile = wp_get_attachment_url($mypod->field('video.ID'));
                            $videoPoster = wp_get_attachment_url($mypod->field('video_poster.ID'));
                        ?>

                        <?php if ($videoFile) : ?>
                            <div class="swiper-slide swiper-no-swiping">
                                <div class="videoWrapper">
                                    <video controls poster="<?php echo $videoPoster ?>" class="js-player">
                                        <source src="<?php echo $videoFile ?>" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if ($youtubeLink) : ?>
                            <div class="swiper-slide swiper-no-swiping">
                                <div class="videoWrapper">
                                    <div data-type="youtube" data-video-id="<?php parse_str( parse_url( $youtubeLink, PHP_URL_QUERY ), $my_array_of_vars ); echo $my_array_of_vars['v']; ?>" class="js-player"></div>
                                </div>
                            </div>
                        <?php endif; ?>

                    <?php endwhile; ?>
                </div>
            </div>
            <div class="container-fluid">
                <div class="controls">
                    <div class="prevArrow">
                        <div class="arrow left"></div>
                    </div>
                    <div class="pagination"></div>
                    <div class="nextArrow">
                        <div class="arrow right"></div>
                    </div>
                </div>
                <br><br>
                <a class="btn" href="/video-gallery">SEE ALL VIDEOS</a>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
