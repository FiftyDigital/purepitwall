<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head <?php do_action( 'add_head_attributes' ); ?>>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="theme-color" content="#fe5000">
    <link rel="icon" type="image/x-icon" href="<?php echo get_template_directory_uri();?>/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
    <link rel='dns-prefetch' href='https://www.mercedesamgf1.com/en/mercedes-amg-f1/purepitwall/?page=ppw'>
    <?php wp_head(); ?>
</head>
<body style="background:black;">
<div class="PPW_panelMain liveSocialPanelFeed">
    <?php echo do_shortcode('[ff id="3"]') ?>
</div>

<?php wp_footer(); ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/main.min.js"></script>
<script id="__bs_script__">
    if (window.location.href.indexOf("localhost") > -1){
        //<![CDATA[
            document.write("<script async src='https://HOST:3000/browser-sync/browser-sync-client.js?v=2.18.8'><\/script>".replace("HOST", location.hostname));
        //]]>
    }
</script>
<!-- <script type="text/javascript">_satellite.pageBottom();</script> -->
</body>
</html>
