<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head <?php do_action( 'add_head_attributes' ); ?>>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="theme-color" content="#fe5000">
    <link rel="icon" type="image/x-icon" href="<?php echo get_template_directory_uri();?>/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
    <link rel='dns-prefetch' href='https://www.mercedesamgf1.com/en/mercedes-amg-f1/purepitwall/?page=ppw'>
    <?php wp_head(); ?>
    <script>
       var digitalData = {"aa_rprtsute_id":"purestorage-prod","language_code":"en_uk","page_name":"<?php if ( is_front_page() ) { $pageTitle = 'Home'; } else { $pageTitle = wp_title('|', true, 'right');};  echo $pageTitle; ?>","site_id":"purepitwall","site_is_production":"true","page_path":"<?php echo $pageTitle ?>","page_url":"<?php $current_url="//".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; echo $current_url; ?>"};
    </script>
    <script src="//assets.adobedtm.com/ae4adead2b13fe03bb8e4664da768dfd3674614d/satelliteLib-86794e05355b62fc9250926a208e16b22853239a.js"></script>
</head>
<body <?php body_class(); ?>>
<div id="wrapper" class="hfeed">
    <header id="header">
        <div class="headerLogo">
            <a href="<?php echo get_home_url(); ?>">
                <img src="<?php echo get_template_directory_uri();?>/assets/img/purepitwall_logo.svg" class="pp_logo">
            </a>
        </div>
        <div class="menuToggle">
            <div class="toggleButton">
                <div class="toggleLine"></div>
                <div class="toggleLine"></div>
                <div class="toggleLine"></div>
            </div>
        </div>
        <div class="menuContainer">
            <nav id="menu" role="navigation">
                <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
            </nav>
        </div>

        <div class="menuContainerMobile">
            <div class="menuContainerBg"></div>
            <nav id="menu" role="navigation">
                <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
            </nav>
        </div>
    </header>
    <div id="container">
        <?php if ( is_user_logged_in() ) {
            // your code for logged in user
            ?>
            <?php
                $current_user = wp_get_current_user();
            ?>
            <div class="loggedInBar">
                <div class="barContent">
                    <a href="<?php echo get_site_url(); ?>/podiumpredictor">
                        Welcome back, <h4 class="orange"><?php echo esc_html( $current_user->user_login ); ?></h4>
                        <div class="userAvatarMini"><?php um_fetch_user( get_the_author_meta('ID')); $size = '40'; $avatar = um_user('profile_photo', $size); echo $avatar;?></div>
                    </a>
                </div>
            </div>
            <?php } ?>
