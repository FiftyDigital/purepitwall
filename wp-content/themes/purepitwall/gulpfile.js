var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');
var rename = require("gulp-rename");
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var gutil = require('gulp-util');
var cleanCSS = require('gulp-clean-css');
var assign = require('lodash.assign');
var watchify = require('watchify');

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    return gulp.src("scss/*.scss")
        .pipe(sass())
        .pipe(cleanCSS())
        .pipe(gulp.dest(""))
        .pipe(browserSync.stream());
});

// browserify
var customOpts = {
  entries: ['js/main.js'],
  debug: true
};
var opts = assign({}, watchify.args, customOpts);
var b = watchify(browserify(opts));

gulp.task('javascript', bundle);
b.on('update', bundle);
b.on('log', gutil.log);

function bundle() {
  return b.bundle()
    // log errors if they happen
    .on('error', gutil.log.bind(gutil, 'Browserify Error'))
    .pipe(source('main.min.js'))

    // optional, remove if you don't need to buffer file contents
    .pipe(buffer())
    // optional, remove if you dont want sourcemaps
    .pipe(sourcemaps.init({loadMaps: true})) // loads map from browserify file
       // Add transformation tasks to the pipeline here.
    .pipe(uglify({compress:true}))
    .pipe(sourcemaps.write('./')) // writes .map file
    .pipe(gulp.dest('js/'));
}


// Static Server + watching scss/html files
gulp.task('serve', ['sass','javascript'], function() {

    browserSync.init({
        notify: true, // hide 'Connected to Browsersync' notification
        proxy: "https://purepitwall.local9"
    });

    gulp.watch("scss/**/**.scss*", ['sass']);
    gulp.watch(["js/**/*.js","!js/*.min.*"], ['javascript']);
    //gulp.watch("*.css").on('change', browserSync.reload);
    gulp.watch("*.php").on('change', browserSync.reload);
    gulp.watch("assets/*.*").on('change', browserSync.reload);
    gulp.watch("js/main.min.js").on('change', browserSync.reload);
});

gulp.task('default', ['serve']);
