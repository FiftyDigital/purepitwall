<?php
    /** ACTUAL Home Page */
?>

<?php get_header(); ?>
<section class="homePage" role="main">

    <div class="homeTop">
        <?php /* Start the Pods Loop */
            // Pulling PODS Loop instead of WP Loop
            $mypod = pods('home_hero_video');
            $params = array(
                'limit' => 1,
                'orderby' => 'date DESC'
                );
            $mypod->find($params);
        ?>
        <?php while ( $mypod->fetch() ) : ?>
            <?php
                $videoFile = wp_get_attachment_url($mypod->field('video_file.ID'));
                $videoPoster = wp_get_attachment_url($mypod->field('video_fallback_image.ID'));
            ?>
            <div class="videoBg js-bgImg" style="background-image: url('<?php echo $videoPoster ?>')">
                <video class="bg-video" data-url="<?php echo $videoFile;?>" playsinline autoplay loop muted></video>
            </div>
        <?php endwhile; ?>

        <div class="videoBgOverlay"></div>
        <div class="panelContent">
            <div class="container-fluid">
                <div class="row no-gutter">
                    <div class="col-xs-12 ">
                        <div class="shadowLogoHolder">
                            <div class="shadowLogo shadow">
                                <img src="<?php echo get_template_directory_uri();?>/assets/img/logoshadow.png">
                            </div>
                            <div class="shadowLogo">
                                <img src="<?php echo get_template_directory_uri();?>/assets/img/purepitwall_logo.svg">
                            </div>
                        </div>
                        <div class="raceCountdown">
                            <h5>Get the latest stats and strategy as the race unfolds</h5>
                            <div class="countdownHolder js-raceCountDown">
                                <h3 class="raceName"></h3>
                                <div class="raceTime"></div>
                            </div>
                            <div class="pitwallCTA">
                                <?php wp_nav_menu( array( 'menu' => 'pitwallcta' ) ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="homeBottom">
        <div class="container-fluid">
            <div class="row">
                <h3>It isn't cheating, but it definitely feels that way...</h3>
            </div>

            <div class="row">
                <ul>
                    <a href="<?php echo get_site_url(); ?>/unfairadvantage">
                        <li>
                            <div class="homeIconLegend">
                                <h4><span class="firstWord">#Unfair</span> <span class="secondWord">Advantage</span></h4>
                            </div>
                            <div class="homeIconHolder">
                                <div class="homeIcon" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/home_unfairadvantage.jpg')">
                                </div>
                            </div>
                            <div class="homeIconCopy">
                                Find out how Pure Storage gives Mercedes-AMG Petronas Motorsport an #UnfairAdvantage.
                            </div>
                        </li>
                    </a>
                    <a href="<?php echo get_site_url(); ?>/livesocial">
                        <li>
                            <div class="homeIconLegend">
                                <h4><span class="firstWord">Live</span> <span class="secondWord">Social</span></h4>
                            </div>
                            <div class="homeIconHolder">
                                <div class="homeIcon" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/home_livesocial.jpg')">
                                </div>
                            </div>
                            <div class="homeIconCopy">
                                All the latest live social media updates from @PurePitWall and @MercedesAMGF1.
                            </div>
                        </li>
                    </a>
                    <a href="<?php echo get_site_url(); ?>/purepitwall">
                        <li>
                            <div class="homeIconLegend">
                                <h4><span class="firstWord">Pure</span> <span class="secondWord">Pitwall</span></h4>
                            </div>
                            <div class="homeIconHolder">
                                <div class="homeIcon" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/home_pitwall.jpg')">
                                </div>
                            </div>
                            <div class="homeIconCopy">
                                Follow on race day for the latest stats and strategy as the race unfolds.
                            </div>
                        </li>
                    </a>
                    <a href="<?php echo get_site_url(); ?>/podiumpredictor">
                        <li>
                            <div class="homeIconLegend">
                                <h4><span class="firstWord">Podium</span> <span class="secondWord">Predictor</span></h4>
                            </div>
                            <div class="homeIconHolder">
                                <div class="homeIcon" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/home_predictor.jpg')">
                                </div>
                            </div>
                            <div class="homeIconCopy">
                               Sign up to win exclusive prizes if you accurately predict the results of each Grand Prix.
                            </div>
                        </li>
                    </a>
                    <a href="<?php echo get_site_url(); ?>/influencerinsights">
                        <li>
                            <div class="homeIconLegend">
                                <h4><span class="firstWord">Influencer</span> <span class="secondWord">Insights</span></h4>
                            </div>
                            <div class="homeIconHolder">
                                <div class="homeIcon" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/home_influencer.jpg')">
                                </div>
                            </div>
                            <div class="homeIconCopy">
                                The best live updates from superfans and key F1™ influencers.
                            </div>
                        </li>
                    </a>
                    <a href="<?php echo get_site_url(); ?>/racenotes">
                        <li>
                            <div class="homeIconLegend">
                                <h4><span class="firstWord">Race</span> <span class="secondWord">Notes</span></h4>
                            </div>
                            <div class="homeIconHolder">
                                <div class="homeIcon" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/home_racenotes.jpg')">
                                </div>
                            </div>
                            <div class="homeIconCopy">
                                Read our post-race blog and discover the latest developments from the world of Pure Pit Wall.
                            </div>
                        </li>
                    </a>
                    <a href="<?php echo get_site_url(); ?>/video-gallery">
                        <li>
                            <div class="homeIconLegend">
                                <h4><span class="firstWord">Video</span> <span class="secondWord">Gallery</span></h4>
                            </div>
                            <div class="homeIconHolder">
                                <div class="homeIcon" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/home_videogallery.jpg')">
                                </div>
                            </div>
                            <div class="homeIconCopy">
                                All Pure Pit Wall analysis videos and the best #UnfairAdvantage highlights in one place.
                            </div>
                        </li>
                    </a>

                </ul>
            </div>

        </div>
    </div>
    <div class="home_product">
        <div class="box box_left" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/home_product_left.jpg')">
        </div>
        <div class="box box_center">
            <div class="box_content">
                <div class="homeProductTitle">NOW EVERY ORGANIZATION<br>CAN ACCELERATE</div>
                <div class="homeProductCopy">See how you can be <span class="orange">fast and agile everywhere</span>, and glean intelligence from analyzing data, at massive scale, in real-time</div>
                <a class="btn btn-white" href="http://www.purestorage.com/uk/products.html" target="new">SEE PRODUCTS</a>
            </div>
        </div>
        <div class="box box_right" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/home_product_right.jpg')">
        </div>
    </div>


</section>
<?php get_footer(); ?>
