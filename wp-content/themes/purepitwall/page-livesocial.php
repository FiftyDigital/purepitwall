<?php get_header(); ?>
<section class="sitePage liveSocial" role="main">
    <div class="pageHero">
        <?php $id = get_queried_object_id(); if (has_post_thumbnail( $id) ): ?>
            <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'single-post-thumbnail' ); ?>
                <div class="pageHeroBg js-bgImg" style="background-image: url('<?php echo $image[0]; ?>')">
                </div>
        <?php endif; ?>
        <div class="pageHeroBgOverlay"></div>
        <div class="pageHeroContent">
            <div class="container-fluid">
                <div class="row no-gutter">
                    <h1><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
    </div>

    <div class="pageContent">
        <div class="container-fluid">
            <div class="row no-gutter">
                <div class="">
                    <!-- pageBody -->
                    <div class="row no-gutter">
                        <div class="intro col-xs-12 col-lg-8">
                            <h2>What we're saying</h2>
                        </div>
                        <div class="col-lg-4">
                        </div>
                    </div>

                    <div class="row no-gutter">

                        <div class="col-xs-12 col-lg-8 intro ">
                            <h3 class="columnTitle">@PURE<span class="secondWord">PITWALL</span></h3>
                            <div id="streamHolder" class="streamHolder">
                                <?php echo do_shortcode('[ff id="2"]') ?>
                            </div>
                        </div>

                        <div class="col-xs-12 col-lg-4 pageSideBar">
                            <div class="sideBarContent">
                                <h3 class="columnTitle">Mercedes-amg petronas motorsport</span></h3>

                                <div class="ls_postsHolder">

                                     <?php /* Start the Pods Loop */
                                        // Pulling PODS Loop instead of WP Loop
                                        $mypod = pods('livesocial_post');
                                        $params = array(
                                            'limit' => 6,
                                            'orderby' => 'date DESC'
                                            );
                                        $mypod->find($params);
                                    ?>

                                    <?php while ( $mypod->fetch() ) : ?>
                                        <?php

                                            $livesocial_poster = $mypod->field('poster_user');
                                            $livesocial_content = $mypod->field('poster_text');
                                            $livesocial_posterUser = $mypod->display('poster_user');
                                            $livesocial_postdate = $mypod->field('post_date');
                                        ?>

                                        <div class="ls_postContainer <?php echo $livesocial_poster; ?>">
                                            <div class="ls_postImage <?php echo $livesocial_poster; ?>"></div>
                                            <div class="ls_postContent">
                                                <h3 class="ls_postUser"><?php echo $livesocial_posterUser; ?> <span class="ls_postMeta">
                                                    <?php echo human_time_diff(strtotime($livesocial_postdate), current_time('timestamp')) .' ago'; ?></span></h3>
                                                <p><?php echo $livesocial_content; ?></p>
                                            </div>

                                        </div>
                                    <?php endwhile; ?>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>


</section>
<?php get_footer(); ?>
