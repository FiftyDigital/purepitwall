<?php
    /** Real RACE NOTES page; needed this to setup the section correctly, WP quirk*/
?>


<?php get_header(); ?>
<section class="sitePage raceNotes" role="main">

    <div class="pageHero">
        <?php $id = get_queried_object_id(); if (has_post_thumbnail( $id) ): ?>
            <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'single-post-thumbnail' ); ?>
                <div class="pageHeroBg js-bgImg" style="background-image: url('<?php echo $image[0]; ?>')">
                </div>
        <?php endif; ?>
        <div class="pageHeroBgOverlay"></div>
        <div class="pageHeroContent">
            <div class="container-fluid">
                <div class="row no-gutter">
                    <h1><?php $pageTitle = get_the_title( get_option('page_for_posts', true) ); echo $pageTitle; ?></h1>
                </div>
            </div>
        </div>
    </div>


    <div class="pageContent">
        <div class="container-fluid">
            <div class="row no-gutter">
                <div class="pageBody col-xs-12">
                    <!-- pageBody -->
                    <div class="row no-gutter">
                        <div class="intro col-xs-12">
                            <h2>Explore In Depth</h2>
                            <p>Read in detail how each Grand Prix unfolds with our post-race blog and discover new, exciting developments from the data-driven world of Pure Pit Wall.</p>
                        </div>
                    </div>
                    <div class="row no-gutter">
                        <div class="articlesContainer">

                            <?php // Display blog posts on any page @ http://m0n.co/l
                            $i = 1;
                            echo '<div class="postArticleGroup">';
                            $temp = $wp_query; $wp_query= null;
                            $wp_query = new WP_Query(); $wp_query->query('showposts=20' . '&paged='.$paged);
                            while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

                            <div class="postArticle">
                                <a href="<?php the_permalink(); ?>" title="Read more">
                                    <div class="postArticleImage">
                                        <?php if (has_post_thumbnail( $post->ID ) ): ?>
                                          <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                                          <div class="imageWrapper">
                                              <div class="imageContainer js-bgImg" style="background-image: url('<?php echo $image[0]; ?>')">
                                              </div>
                                          </div>
                                        <?php endif; ?>
                                    </div>
                                    <div class="postArticleTitle">
                                        <h2><?php the_title(); ?></h2>
                                        <div class="postDate"><?php echo get_the_date( 'd.m.Y' ); ?></div>
                                    </div>
                                </a>
                            </div>

                            <?php if ($i % 10 == 0){ echo '</div><div class="postArticleGroup">';};
                            $i++; ?>


                            <?php endwhile; ?>
                            <?php wp_reset_postdata(); ?>


                            <?php echo '</div>'; ?>
                        </div>

                        <div class="loadMore">
                            <?php load_more_button() ?>
                        </div>



                    </div>
                    <!-- end of pageBody -->
                </div>
            </div>
        </div>

    </div>

</section>
<?php get_footer(); ?>
