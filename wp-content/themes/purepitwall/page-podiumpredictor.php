<?php get_header(); ?>
<section class="sitePage podiumPredictor predictorMainPage <?php if (is_user_logged_in() ) {echo 'logged-in'; }; ?>" role="main">

    <div class="pageHero">
        <?php $id = get_queried_object_id(); if (has_post_thumbnail( $id) ): ?>
            <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'single-post-thumbnail' ); ?>
                <div class="pageHeroBg js-bgImg" style="background-image: url('<?php echo $image[0]; ?>')">
                </div>
        <?php endif; ?>
        <div class="pageHeroBgOverlay"></div>
        <div class="pageHeroContent">
            <div class="container-fluid">
                <div class="row no-gutter">
                    <div class="col-xs-12 col-lg-6 titleHolder">
                        <h1><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="pageContent top">
        <div class="container-fluid">

            <?php if ( !is_user_logged_in() ) { ?>
                <div class="signInUpContainer">
                    <div class="signUpPanel">
                        <div class="signUpCopy">
                            We love stats and predictions, so for the 2018 season we thought we would give you the chance of winning awesome and exclusive prizes by predicting the season's race results. Sign up and get predicting!
                        </div>
                        <div class="signUpLink">
                            <a class="btn" href="<?php echo get_site_url(); ?>/register">
                                SIGN UP NOW
                            </a>
                        </div>
                    </div>

                    <div class="signInPanel">
                        <h2>SIGN IN</h2>
                        <?php echo do_shortcode('[ultimatemember form_id=315]'); ?>
                        <p>OR</p>
                        <?php echo do_shortcode('[ultimatemember_social_login id=337]'); ?>
                        <?php echo do_shortcode('[ultimatemember_social_login id=319]'); ?>
                    </div>
                </div>
            <?php }?>

            <?php if ( is_user_logged_in() ) {
                // your code for logged in user
                ?>
                <?php
                    $current_user = wp_get_current_user();
                ?>
                <div class="row no-gutter">
                    <div class="col-xs-12 col-md-push-9 col-md-3">
                        <div class="profileArea">
                            <?php if ( !um_is_on_edit_profile() ) { ?>
                                <div class="avatarContainer">
                                <?php um_fetch_user( get_the_author_meta('ID')); $size = '220'; $avatar = um_user('profile_photo', $size); echo $avatar;?>
                                </div>
                                <h3><?php echo esc_html( $current_user->user_login ); ?></h3>
                                <a class="profileLink" href="<?php echo get_site_url(); ?>/podiumpredictor/?profiletab=main&um_action=edit">Edit Photo</a><br>
                                <a class="profileLink" href="<?php echo get_site_url(); ?>/account">My Account</a><br>
                                <a class="profileLink" href="<?php echo wp_logout_url(get_permalink()); ?>">Logout</a>
                            <?php } ?>

                            <?php if ( um_is_on_edit_profile() ) { ?>
                                <?php echo do_shortcode('[ultimatemember form_id=323]'); ?>

                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-9 col-md-pull-3 ">
                        <div class="row no-gutter">
                            <div class="col-xs-12 col-sm-6 col-lg-8">
                                <div class="pred_titleHolder">
                                    <h2>Predict the race results</h2>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-lg-4 selectHolder">
                                <!-- raceSelect -->
                                <div class="raceDisplay topD">
                                    <div class="raceSelect">
                                        <div class="currentRace">
                                            <div class="optionFlag"></div>
                                            <div class="currentRaceName"></div>
                                        </div>
                                        <div class="arrow">
                                            <div class="Icon Icon--angle-down"></div>
                                        </div>
                                        <div class="raceDropDownHolder">
                                            <div class="raceDropDown"></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end of raceSelect -->
                            </div>
                        </div>
                        <div class="row no-gutter">
                            <!-- raceDisplay -->
                            <div class="raceDisplay bottomD">
                                <div class="raceData">
                                    <div class="raceTime"></div>
                                    <div class="raceTimeLegend">* You can change your race prediction at any time before this. The cutoff time is before qualifying.</div>
                                    <div class="raceMap"></div>
                                    <h3 class="raceName"></h3>
                                    <h4 class="raceWeekend orange"></h4>
                                    <h5 class="raceCircuit"></h5>
                                    <h5 class="raceLocation"></h5>
                                </div>
                            </div>
                             <!-- end of raceDisplay -->
                        </div>
                    </div>
                </div>

            <?php } ?>

            <!-- Podium Predictor Main Interface -->
            <div class="row no-gutter mainPredictorInterface">
                <h3>Make your selections</h3>
                <div class="predictorContainer">
                    <div class="driverOptions">
                    </div>
                    <div class="predictorForm">

                        <div class="driverInfo">
                            <div class="driverInfoContent">
                                <div class="driverPictureHolder">
                                    <div class="driverPicture"></div>
                                </div>
                                <div class="driverData">
                                    <h3 class="driverName"></h3>
                                    <h5 class="driverTeam"></h5>

                                    <!-- <h5>Car Number</h5>
                                    <h3 class="data driverCarNumber"></h3> -->

                                    <h5>D.O.B</h5>
                                    <h3 class="data driverDOB"></h3>

                                    <h5>Nationality</h5>
                                    <h3 class="data driverNationality"></h3>

                                    <h5>Height</h5>
                                    <h3 class="data driverHeight"></h3>

                                    <h5>First GP</h5>
                                    <h3 class="data driverFirstGP"></h3>

                                </div>
                            </div>
                        </div>

                        <div class="predictorOptions">

                            <?php if ( is_user_logged_in() ) {?>
                            <h3><?php echo esc_html( $current_user->user_login ); ?>'s Podium Selections</h3>
                            <?php } else { ?>
                            <h3>Your Podium Selections</h3>
                            <?php } ?>



                            <h5>Click a position and select the driver to make a race result prediction. Get extra 10 pts. for correctly predicting all 3 positions, or 5 pts. for predicting 2.</h5>
                            <div class="predictorOptionsHolder top">
                                <div class="js-position position-1" data-driver="">
                                    <div class="positionLegend">1<sup>ST</sup></div>
                                    <div class="positionAvatar"></div>
                                    <div class="positionPoints">+25pts</div>
                                </div>
                                <div class="js-position position-2" data-driver="">
                                    <div class="positionLegend">2<sup>ND</sup></div>
                                    <div class="positionAvatar"></div>
                                    <div class="positionPoints">+18pts</div>
                                </div>
                                <div class="js-position position-3" data-driver="">
                                    <div class="positionLegend">3<sup>RD</sup></div>
                                    <div class="positionAvatar"></div>
                                    <div class="positionPoints">+15pts</div>
                                </div>

                            </div>

                            <h3 class="bonusPoints">BONUS POINTS</h3>
                            <div class="predictorOptionsHolder bottom">
                                <div class="pred_OptionItem row1 js-position qualiPole" data-driver="">
                                    <div class="bonusLegend">
                                        <div class="bonusIcon time"></div>
                                        <h5>QUALIFYING POLE</h5>
                                    </div>
                                    <div class="entry">
                                        <div class="positionAvatar"></div>
                                    </div>
                                    <div class="positionPoints">+15pts</div>
                                </div>
                                <div class="pred_OptionItem row1 qualiTime">
                                    <div class="bonusLegend">
                                        <div class="bonusIcon time"></div>
                                        <h5>QUALIFYING POLE TIME</h5>
                                    </div>
                                    <div class="entry">
                                        <div class="entryInner">
                                            <p class="supLegend">Last Year: <span class="lastYearPoleTime"></span></p>

                                            <span class="pred_MM"><input id="pred_MM" type="text" maxlength="2" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" onkeypress='return event.charCode >= 48 && event.charCode <= 57'></span>
                                            <span>
                                                <div class="plusMinus plusMinus_MM">
                                                    <button type="button" class="plusMinus_but plusMinus_plus">
                                                        +
                                                    </button>
                                                    <button type="button" class="plusMinus_but plusMinus_minus">
                                                        -
                                                    </button>
                                                </div>
                                            </span>
                                            :
                                            <span class="pred_SS"><input id="pred_SS" type="text" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" onkeypress='return event.charCode >= 48 && event.charCode <= 57'></span>
                                            <span>
                                                <div class="plusMinus plusMinus_SS">
                                                    <button type="button" class="plusMinus_but plusMinus_plus">
                                                        +
                                                    </button>
                                                    <button type="button" class="plusMinus_but plusMinus_minus">
                                                        -
                                                    </button>
                                                </div>
                                            </span>
                                            .
                                            <span class="pred_CC"><input id="pred_CC" type="text" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="3" onkeypress='return event.charCode >= 48 && event.charCode <= 57'></span>
                                            <span>
                                                <div class="plusMinus plusMinus_CC">
                                                    <button type="button" class="plusMinus_but plusMinus_plus">
                                                        +
                                                    </button>
                                                    <button type="button" class="plusMinus_but plusMinus_minus">
                                                        -
                                                    </button>
                                                </div>
                                            </span>

                                          </div>
                                    </div>
                                    <div class="positionPoints">+20pts within 0.25% accuracy<br>+10pts within 0.5% accuracy<br>+5pts within 1% accuracy</div>
                                </div>
                                <div class="pred_OptionItem row2 safetyCar">
                                    <div class="bonusLegend">
                                        <div class="bonusIcon safety"></div>
                                        <h5>SAFETY CAR</h5>
                                    </div>
                                    <div class="entry switch">
                                        <div class="customCheckbox">
                                            <input type="checkbox" value="1" id="pred_Safety" name="" />
                                            <label for="pred_Safety"></label>
                                            <div></div>
                                        </div>
                                    </div>
                                    <div class="positionPoints">+5pts</div>
                                </div>
                                <div class="pred_OptionItem row2 raceRain">
                                    <div class="bonusLegend">
                                        <div class="bonusIcon safety"></div>
                                        <h5>RAIN</h5>
                                    </div>
                                    <div class="entry switch">
                                        <div class="customCheckbox">
                                            <input type="checkbox" value="1" id="pred_Rain" name="" />
                                            <label for="pred_Rain"></label>
                                            <div></div>
                                        </div>
                                    </div>
                                    <div class="positionPoints">+5pts</div>
                                </div>
                                <div class="pred_OptionItem row2 DNF">
                                    <div class="bonusLegend">
                                        <div class="bonusIcon dnf"></div>
                                        <h5>DNF</h5>
                                    </div>
                                    <div class="entry">
                                        <div class="entryInner">
                                            <span class="pred_DNF"><input id="pred_DNF" type="text" maxlength="2" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" onkeypress='return event.charCode >= 48 && event.charCode <= 57'></span>

                                            <span>
                                                <div class="plusMinus plusMinus_DNF">
                                                    <button type="button" class="plusMinus_but plusMinus_plus">
                                                        +
                                                    </button>
                                                    <button type="button" class="plusMinus_but plusMinus_minus">
                                                        -
                                                    </button>
                                                </div>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="positionPoints">+10pts</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="predictorSubmit">
                        <input type="submit" value="Add Prediction" id="pred_Submit" class="disabled">
                    </div>
                    <div class="messageAlert">
                        <div class="msgContent">
                            <h5>Your prediction has been saved!</h5>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of Podium Predictor Main Interface -->
            <!-- racingPlugin -->
            <?php if ( is_user_logged_in() ) { ?>
            <div class="racingPlugin">
                <?php echo do_shortcode('[motorracingleague entry=2]') ?>
            </div>
            <?php } ?>
            <!-- end of racingPlugin -->
        </div>
    </div>

    <div class="pagePromoHolder_2018">
        <div class="container-fluid">
            <h1 class="prizesTitle"><span class="unbold">2018</span><span class="orange">PRIZES</span></h1>
            <h3 class="prizesLine"><span class="unbold">IN 2018 THERE ARE <span class="orange">TWO WAYS</span> YOU CAN WIN <span class="orange">TOP PRIZES</span> WITH THE PODIUM PREDICTOR!</span></h3>
            <div class="boxHolder">
                <div class="box">
                    <h2 class="boxTitle"><span class="unbold"><span class="orange">RACE</span>BY<span class="orange">RACE</span>PRIZES</span></h2>
                    <div class="boxCopy">THE WINNER OF EACH RACE WILL GET ONE OF A SELECTION OF AWESOME PRIZES!</div>
                    <div class="prizeHolder">
                        <div class="raceByRace" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/predictor_2018_racebyrace.jpg')">
                        </div>
                    </div>
                </div>
                <div class="box">
                    <h2 class="boxTitle"><span class="unbold"><span class="orange">SEASON</span>PRIZES</span></h2>
                    <div class="boxCopy">AT THE END OF THE SEASON, THE TOP 20 SCORING PLAYERS FROM EACH RACE WEEKEND GET ENTERED INTO A RAFFLE DRAW AND THE FIRST THREE TO BE SELECTED WIN THE TOP PRIZES!</div>
                    <div class="prizeHolder">
                        <div class="prizeBox">
                            <div class="prizeSquare" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/predictor_2018_prize1.jpg')">
                            </div>
                            <div class="medal" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/medal_gold.png')"></div>
                            <div class="prizeName"> Austin Race Weekend Experience For Two</span></div>
                        </div>
                        <div class="prizeBox">
                            <div class="prizeSquare" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/predictor_2018_prize2.jpg')">
                            </div>
                            <div class="medal" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/medal_silver.png')"></div>
                            <div class="prizeName">Pure Storage Accelerate Event 2019</span></div>
                        </div>
                        <div class="prizeBox">
                            <div class="prizeSquare" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/predictor_2018_prize3.jpg')">
                            </div>
                            <div class="medal" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/medal_bronze.png')"></div>
                            <div class="prizeName">Mercedes-AMG Petronas Motorsport Race Support Room Experience</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="pageContent bottom">
        <div class="container-fluid">
            <div class="leaderBoardHolder">

                <div class="mainLeaderBoardPanel">
                    <div class="row leaderBoardSelect">
                        <h2>Leaderboard</h2>
                        <div class="raceDisplay leadBD">
                            <div class="raceSelect">
                                <div class="currentRace">
                                    <div class="optionFlag"></div>
                                    <div class="currentRaceName"></div>
                                </div>
                                <div class="arrow">
                                    <div class="Icon Icon--angle-down"></div>
                                </div>
                                <div class="raceDropDownHolder">
                                    <div class="raceDropDown"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panelContent statsContainer">
                        <div class="statsHolder Overall selected">
                            <?php echo do_shortcode('[motorracingleague results=2 used_doubleups=0]') ?>
                        </div>
                        <div class="statsHolder AustralianGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=21]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=21]') ?>
                            </div>
                        </div>

                        <div class="statsHolder BahrainGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=22]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=22]') ?>
                            </div>
                        </div>

                        <div class="statsHolder ChineseGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=23]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=23]') ?>
                            </div>
                        </div>

                        <div class="statsHolder AzerbaijanGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=24]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=24]') ?>
                            </div>
                        </div>

                        <div class="statsHolder SpanishGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=25]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=25]') ?>
                            </div>
                        </div>

                        <div class="statsHolder MonacoGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=26]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=26]') ?>
                            </div>
                        </div>

                        <div class="statsHolder CanadianGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=27]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=27]') ?>
                            </div>
                        </div>

                        <div class="statsHolder FrenchGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=28]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=28]') ?>
                            </div>
                        </div>

                        <div class="statsHolder AustrianGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=29]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=29]') ?>
                            </div>
                        </div>

                        <div class="statsHolder BritishGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=30]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=30]') ?>
                            </div>
                        </div>

                        <div class="statsHolder GermanGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=31]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=31]') ?>
                            </div>
                        </div>

                        <div class="statsHolder HungarianGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=32]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=32]') ?>
                            </div>
                        </div>

                        <div class="statsHolder BelgianGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=33]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=33]') ?>
                            </div>
                        </div>

                        <div class="statsHolder ItalianGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=34]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=34]') ?>
                            </div>
                        </div>

                        <div class="statsHolder SingaporeGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=35]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=35]') ?>
                            </div>
                        </div>

                        <div class="statsHolder RussianGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=36]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=36]') ?>
                            </div>
                        </div>

                        <div class="statsHolder JapaneseGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=37]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=37]') ?>
                            </div>
                        </div>

                        <div class="statsHolder USAGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=38]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=38]') ?>
                            </div>
                        </div>

                        <div class="statsHolder MexicanGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=39]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=39]') ?>
                            </div>
                        </div>

                        <div class="statsHolder BrazilGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=40]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=40]') ?>
                            </div>
                        </div>

                        <div class="statsHolder AbuDhabiGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=41]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=41]') ?>
                            </div>
                        </div>
                    </div>
                </div>



                <?php if ( is_user_logged_in() ) { ?>
                    <div class="mypredictionsHolder">
                        <h2><?php echo esc_html( $current_user->user_login ); ?>'s Predictions</h2>
                        <div class="statsHolder">
                            <?php echo do_shortcode('[motorracingleague predictions=2 limit=25]') ?>
                        </div>
                    </div>
                <?php } ?>

            </div>

        </div>
    </div>
</section>
<?php get_footer(); ?>
