<?php get_header(); ?>
<style>
    body {
        background: black !important;
    }
</style>
<section class="sitePage purePitwall PPW_panelMain <?php if (is_user_logged_in() ) {echo 'logged-in'; }; ?>" role="main">
    <div class="notificationBar">
        <div class="bar">
            <div class="barContent">
                <h4 class="notificationText"><span class="white glow"><span class="Icon Icon--twitter"></span> NEW UPDATE!</span> SEE THE LATEST LIVE SOCIAL FEED HERE</h4>
            </div>
            <div class="barClose">
                <span class="Icon Icon--close"></span>
            </div>
        </div>
    </div>
    <div class="pitwallContainer">
        <iframe class="pitwall" src="https://www.mercedesamgf1.com/en/mercedes-amg-f1/purepitwall/?page=ppw"></iframe>
    </div>

    <!-- predictor panel -->
    <div class="PP_predictorPanel panel_active">
        <div class="panelHolder">
            <div class="panelToggleBar">
                <div class="toggleCopy">
                    <h3 class="toggleTitle">
                        Your Predictions
                    </h3>
                </div>
            </div>
            <div class="panelContentHolder">
                <div class="panelContent">


                    <div class="panel leftPanel">

                        <div class="PP_panelMain">

                            <div class="mainPredictorInterface">
                                <div class="predictorContainer">

                                    <div class="predictorForm">
                                        <div class="predictorOptions">
                                            <?php if ( is_user_logged_in() ) { ?>
                                                <h3  class="bonusPoints"><?php echo esc_html( $current_user->user_login ); ?>'s PODIUM SELECTIONS</h3>
                                            <?php } else { ?>
                                                <h3  class="bonusPoints">YOUR PODIUM SELECTIONS</h3>
                                            <?php }?>
                                            <div class="predictorOptionsHolder top">
                                                <div class="js-position position-1" data-driver="">
                                                    <div class="positionLegend">1<sup>ST</sup></div>
                                                    <div class="positionAvatar"></div>
                                                </div>
                                                <div class="js-position position-2" data-driver="">
                                                    <div class="positionLegend">2<sup>ND</sup></div>
                                                    <div class="positionAvatar"></div>
                                                </div>
                                                <div class="js-position position-3" data-driver="">
                                                    <div class="positionLegend">3<sup>RD</sup></div>
                                                    <div class="positionAvatar"></div>
                                                </div>
                                            </div>

                                            <h3 class="bonusPoints">BONUS POINTS</h3>
                                            <div class="predictorOptionsHolder bottom">
                                                <div class="pred_OptionItem row1 js-position qualiPole" data-driver="">
                                                    <div class="bonusLegend">
                                                        <div class="bonusIcon time"></div>
                                                        <h5>QUALIFYING POLE</h5>
                                                    </div>
                                                    <div class="entry">
                                                        <div class="positionAvatar"></div>
                                                    </div>
                                                </div>
                                                <div class="pred_OptionItem row1 qualiTime">
                                                    <div class="bonusLegend">
                                                        <div class="bonusIcon time"></div>
                                                        <h5>QUALIFYING POLE TIME</h5>
                                                    </div>
                                                    <div class="entry">
                                                        <div class="entryInner">
                                                            <span class="pred_MM"><input id="pred_MM" type="text" maxlength="2" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" onkeypress='return event.charCode >= 48 && event.charCode <= 57'></span>

                                                            :
                                                            <span class="pred_SS"><input id="pred_SS" type="text" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="2" onkeypress='return event.charCode >= 48 && event.charCode <= 57'></span>

                                                            .
                                                            <span class="pred_CC"><input id="pred_CC" type="text" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="3" onkeypress='return event.charCode >= 48 && event.charCode <= 57'></span>
                                                          </div>
                                                    </div>
                                                </div>
                                                <div class="pred_OptionItem row2 safetyCar">
                                                    <div class="bonusLegend">
                                                        <div class="bonusIcon safety"></div>
                                                        <h5>SAFETY CAR</h5>
                                                    </div>
                                                    <div class="entry switch">
                                                        <div class="customCheckbox">
                                                            <input type="checkbox" value="1" id="pred_Safety" name="" />
                                                            <label for="pred_Safety"></label>
                                                            <div></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="pred_OptionItem row2 raceRain">
                                                    <div class="bonusLegend">
                                                        <div class="bonusIcon safety"></div>
                                                        <h5>RAIN</h5>
                                                    </div>
                                                    <div class="entry switch">
                                                        <div class="customCheckbox">
                                                            <input type="checkbox" value="1" id="pred_Rain" name="" />
                                                            <label for="pred_Rain"></label>
                                                            <div></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="pred_OptionItem row2 DNF">
                                                    <div class="bonusLegend">
                                                        <div class="bonusIcon dnf"></div>
                                                        <h5>DNF</h5>
                                                    </div>
                                                    <div class="entry">
                                                        <div class="entryInner">
                                                            <span class="pred_DNF"><input id="pred_DNF" type="text" maxlength="2" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" onkeypress='return event.charCode >= 48 && event.charCode <= 57'></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="ctaHolder">
                            <a class="btn" href="/podiumpredictor">Go To Predictor</a>
                        </div>

                        <?php if ( is_user_logged_in() ) { ?>
                        <div class="racingPlugin">
                            <div class="predictionsTable"><?php echo do_shortcode('[motorracingleague predictions=2 limit=25]') ?></div>
                        </div>
                        <?php }?>

                    </div>


                    <div class="panel rightPanel <?php if (!is_user_logged_in() ) {echo 'not-logged-in'; }; ?>">
                        <div class="titlePanel">
                            <div>
                                <h3 class="orange">LIVE<span class="secondWord">SOCIAL</span></h3>
                            </div>
                            <div class="socialExt">
                                <h5>Discover Pure Pitwall #UnfairAdvantage on <a href="https://www.linkedin.com/showcase/11059740/" target="new"><img class="linkedInLogo" src="<?php echo get_template_directory_uri();?>/assets/img/linkedInLogo.png"></a></h5>
                            </div>
                            <div class="streamShadow"></div>
                        </div>
                        <div class="streamHolder">
                            <div class="livesSocialStream">
                                <div class="feedDirect" style="display:block;"><?php echo do_shortcode('[ff id="3"]') ?></div>
                                <iframe id="liveSocialIframe" style="border:0;outline:0;margin:0;position:relative;padding:0;height:100%;min-height:500px;display:none;"></iframe>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- end of predictor panel -->

</section>
<?php get_footer(); ?>
