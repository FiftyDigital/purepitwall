<?php get_header(); ?>
<section id="content" role="main" class="sitePage">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="pageHero">
        <?php if (has_post_thumbnail( $post->ID ) ): ?>
            <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                <div class="pageHeroBg js-bgImg" style="background-image: url('<?php echo $image[0]; ?>')">
                </div>
        <?php endif; ?>
        <div class="pageHeroBgOverlay"></div>
        <div class="pageHeroContent">
            <div class="container-fluid">
                <div class="row no-gutter">
                    <h1><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
    </div>
    <div class="pageContent">
        <div class="container-fluid">

            <div class="pageBody col-xs-12">
                <!-- pageBody -->
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <section class="entry-content">
                            <?php the_content(); ?>
                            <div class="entry-links"><?php wp_link_pages(); ?></div>
                        </section>
                    </article>
                    <?php if ( ! post_password_required() ) comments_template( '', true ); ?>
                    <?php endwhile; endif; ?>
                <!-- end of pageBody -->
            </div>
            <!-- pageSideBar -->
            <!-- <div class="pageSideBar col-xs-12 col-md-4">






            </div> -->
            <!-- end of pageSideBar -->
        </div>

    </div>

</section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
