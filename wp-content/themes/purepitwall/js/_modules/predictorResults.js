'use strict';

var PredictorResults = function($target){
    var self = this;
    self.$el = $target;
    self.$resultsSlider = self.$el.find('.raceByRace .resultsSlider');
    self.$resultsHolder = self.$el.find('.raceByRace .resultsSlider .swiper-container');
    self.$resultsWrapper = self.$el.find('.raceByRace .resultsSlider .swiper-wrapper');
    self.$flagsHolder = self.$el.find('.raceByRace .flagsSlider .swiper-container');
    self.$flagsWrapper = self.$el.find('.raceByRace .flagsSlider .swiper-wrapper');
    self.$swiperContainer = self.$el.find('.nextSeason .swiper-container');
    self.$swiperSlides = self.$el.find('.nextSeason .swiper-slide');


    // next season slideshow
    var mySwiper = new Swiper(self.$swiperContainer, {
        speed: 800,
        loop: true,
        grabCursor: true,
        pagination: {
            el: '.pagination',
            clickable: true,
        },
        centeredSlides: true,
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },

    });


    // load winners, build race-by-race
    self.$winners = [];
    $.getJSON(window.siteUrl+"/wp-content/themes/purepitwall/js/2017predictorWinners.json", function(data) {
        $.each(data.winners,function(key,val) {
            self.$winners.push(val);
        });
    })
    .done(function() {
        self.buildWinnerResults(self.$winners);
    })
    .fail(function() {
    })



}

PredictorResults.prototype.buildWinnerResults = function($data){
    var self = this;
    var data = $data;

    for (var i=0;i<data.length;i++) {
        var winner = data[i];
        var avatar;

        if (winner.winnerPic !== "") {
            avatar = window.siteUrl+'/wp-content/themes/purepitwall/assets/predictor/'+winner.winnerPic;
        } else {
            avatar = window.siteUrl+'/wp-content/themes/purepitwall/assets/img/blankUser.png';
        }

        self.$resultsWrapper.append('<div class="swiper-slide"><div class="holder"><div class="avatar"><div class="winnerAvatar" style="background-image:url('+avatar+')"></div></div><div class="winnerTitle"><div class="colTitle">Winner</div><div class="playerName">'+winner.raceWinner+'</div><div class="racePoints">'+winner.racePoints+' PTS</div></div><div class="predictions"><div class="colTitle">Predictions</div><ul><li><div class="subtitle">1<sup>st</sup></div><div class="prediction">'+winner.predictionFirst+'</div><div class="actual">'+winner.actualFirst+'</div></li><li><div class="subtitle">2<sup>nd</sup></div><div class="prediction">'+winner.predictionSecond+'</div><div class="actual">'+winner.actualSecond+'</div></li><li><div class="subtitle">3<sup>rd</sup></div><div class="prediction">'+winner.predictionThird+'</div><div class="actual">'+winner.actualThird+'</div></li></ul></div><div class="poletimes"><div class="colTitle">Qualifying Pole Time</div><div class="prediction">'+winner.predictionTime+'</div><div class="actual">'+winner.actualTime+'</div></div></div></div>');




        self.$flagsWrapper.append('<div class="swiper-slide"><img src="'+window.siteUrl+'/wp-content/themes/purepitwall/assets/predictor/'+winner.raceFlag+'"></div>')

        if (i==data.length-1){
            var resultsTop = new Swiper(self.$resultsHolder, {
              spaceBetween: 10,
              speed: 600,
              grabCursor: true,
              navigation: {
                nextEl: '.raceByRace .nextArrow',
                prevEl: '.raceByRace .prevArrow',
              },
            });


            var resultsBottom = new Swiper(self.$flagsHolder, {
              spaceBetween: 10,
              centeredSlides: true,
              slidesPerView: 'auto',
              touchRatio: 0.2,
              slideToClickedSlide: true,
            });
            resultsTop.controller.control = resultsBottom;
            resultsBottom.controller.control = resultsTop;
        }
    }
};

module.exports = PredictorResults;
