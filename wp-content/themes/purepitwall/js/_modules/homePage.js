'use strict';

var HomePage = function($target){
    var self = this;
    self.$el = $target;

    // video background
    self.$video = self.$el.find('.bg-video');
    self.$videoUrl = self.$video.attr('data-url');
    self.$videoBg = self.$el.find('.videoBg');

    if (isMobile.any){
      self.$video.hide();
    } else {
      self.$videoBg.css('background-image','none');
    }

    self.$video.get(0).volume = 0;
    self.$backgroundVideo = new BackgroundVideo('.bg-video', {
      src: [self.$videoUrl]
    });


    // animate header content in
    self.$logo = self.$el.find('.shadowLogo');
    self.$raceCountdown = self.$el.find('.raceCountdown');

    TweenMax.fromTo(self.$logo,1,{y:100,alpha:0},{y:0,alpha:1,ease:Strong.easeOut,overwrite:true,force3D:true,delay:0.5});
    TweenMax.fromTo(self.$raceCountdown,1,{y:100,alpha:0},{y:0,alpha:1,ease:Strong.easeOut,overwrite:true,force3D:true,delay:0.75});
}

module.exports = HomePage;
