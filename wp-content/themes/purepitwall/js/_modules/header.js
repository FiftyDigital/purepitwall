'use strict';

var Header = function($target){
    // set the element
    var self = this;
    self.$el = $target;
    self.$menuContainerMobile = self.$el.find('.menuContainerMobile');
    self.$menuContainerBg = self.$el.find('.menuContainerBg');
    self.$mobileNav = self.$el.find('.menuContainerMobile nav');
    self.$mobileNavLinks = self.$el.find('.menuContainerMobile nav a');
    self.$toggleButton = self.$el.find('.toggleButton');
    self.isMenuOpen = false;
    self.isAnimating = false;
    self.duration = 0.6;

    self.$toggleButton.on('click',function(e){
        e.preventDefault();
        e.stopPropagation();
        if(!self.isMenuOpen) {
            self.openNav()
        } else {
            self.closeNav()
        }
    });

    $(window).on('resize',function(){
        if(self.isMenuOpen && !self.isAnimating) {
            self.closeNav()
        }
    })


    self.$mobileNavLinks.on('click',function(e){
        e.preventDefault();
        self.closeNav();
        var goTo = this.getAttribute("href");
        setTimeout(function(){
             window.location = goTo;
        },(self.duration*1000));
    });

    // hide
    $(document).click(function(event) {
        if(!$(event.target).closest(self.$mobileNav).length &&
           !$(event.target).is(self.$mobileNav)) {
            if (self.isMenuOpen) {
                self.closeNav();
            }
        }
    })

};


Header.prototype.openNav = function(){
    var self = this;
    self.isAnimating = true;
    self.isMenuOpen = true;
    self.$menuContainerMobile.addClass('active');
    self.$toggleButton.addClass('active');
    TweenMax.to(self.$menuContainerBg,self.duration,{className:'+=active',ease:Strong.easeOut,overwrite:true,force3D:true});
    TweenMax.to(self.$mobileNav,self.duration,{className:'+=active',ease:Strong.easeOut,overwrite:true,onComplete:function(){
        self.isAnimating = false;
    }});
};

Header.prototype.closeNav = function(){
    var self = this;
    self.isAnimating = true;
    self.$toggleButton.removeClass('active');
    TweenMax.to(self.$mobileNav,self.duration,{className:'-=active',ease:Strong.easeIn,overwrite:true});
    TweenMax.to(self.$menuContainerBg,self.duration,{className:'-=active',ease:Strong.easeOut,overwrite:true,force3D:true,delay:self.duration - 0.2,onComplete:function(){
        self.$menuContainerMobile.removeClass('active');
        self.isMenuOpen = false;
        self.isAnimating = false;
    }});
    self.isMenuOpen = false;
};

module.exports = Header;
