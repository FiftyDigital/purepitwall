'use strict';

var PodiumPredictor = function($target){
    var self = this;
    self.$el = $target;
    self.$races = window.$races;
    self.$drivers = window.$drivers;
    self.$currentRace = [];
    self.$currentRaceTime;
    self.$racingPlugin = self.$el.find('.racingPlugin');
    self.$pluginForm = self.$el.find('#motorracingleague_form');
    self.$pluginFormNotice = self.$el.find('.motorracingleague_notice');
    self.$pluginRaces = self.$pluginForm.find('#mrl_race');
    self.$raceDisplaySelect = self.$el.find('.raceDisplay.topD .raceSelect');
    self.$racesSelectCurrent = self.$el.find('.raceDisplay.topD .currentRace');
    self.$raceDropDown = self.$el.find('.raceDisplay.topD .raceDropDown');
    self.$leaderboardDisplaySelect = self.$el.find('.raceDisplay.leadBD .raceSelect');
    self.$leaderboardSelectCurrent = self.$el.find('.raceDisplay.leadBD .currentRace');
    self.$leaderboardDropDown = self.$el.find('.raceDisplay.leadBD .raceDropDown');
    self.$arrowIconLeadeboard = self.$leaderboardDisplaySelect.find('.arrow .Icon');
    self.$statsContainer = self.$el.find('.statsContainer');
    self.$raceData = self.$el.find('.raceData');
    self.$raceDisplayName = self.$el.find('.raceData .raceName');
    self.$raceDisplayWeekend = self.$el.find('.raceData .raceWeekend');
    self.$raceDisplayCircuit = self.$el.find('.raceData .raceCircuit');
    self.$raceDisplayFlag = self.$el.find('.raceData .raceFlag');
    self.$raceDisplayLocation = self.$el.find('.raceData .raceLocation');
    self.$raceDisplayMap = self.$el.find('.raceData .raceMap');
    self.$raceDisplayTime = self.$el.find('.raceData .raceTime');
    self.$arrowIcon = self.$raceDisplaySelect.find('.arrow .Icon');
    self.$lastYearPoleTime = self.$el.find('.lastYearPoleTime');
    self.$raceOptions;
    self.$lastSelectedDriver = [];
    self.$driverInfo = self.$el.find('.driverInfoContent');
    self.$driverOptionsHolder = self.$el.find('.driverOptions');
    self.$driverPicture = self.$el.find('.driverPicture');
    self.$driverName = self.$el.find('.driverName');
    self.$driverTeam = self.$el.find('.driverTeam');
    self.$driverDOB = self.$el.find('.driverDOB');
    //self.$driverCarNumber = self.$el.find('.driverCarNumber');
    self.$driverNationality = self.$el.find('.driverNationality');
    self.$driverHeight = self.$el.find('.driverHeight');
    self.$driverFirstGP = self.$el.find('.driverFirstGP');
    self.$mainPredictorInterface = self.$el.find('.mainPredictorInterface');
    self.$currentSelectPosition = '';
    self.$positionSelect = self.$el.find('.js-position');
    self.$predictorOptionsHolder = self.$el.find('.predictorOptions');
    self.$podiumPlaces = self.$el.find('.position-1, .position-2, .position-3');
    self.$driverPos1 = self.$mainPredictorInterface.find('.js-position.position-1');
    self.$driverPos2 = self.$mainPredictorInterface.find('.js-position.position-2');
    self.$driverPos3 = self.$mainPredictorInterface.find('.js-position.position-3');
    self.$driverQualiPole = self.$mainPredictorInterface.find('.js-position.qualiPole');
    self.$pluginDriverPole = self.$pluginForm.find('#mrl_participant0');
    self.$pluginDriverP1 = self.$pluginForm.find('#mrl_participant1');
    self.$pluginDriverP2 = self.$pluginForm.find('#mrl_participant2');
    self.$pluginDriverP3 = self.$pluginForm.find('#mrl_participant3');
    self.$pluginSubmit = self.$pluginForm.find('#motorracingleague-add');
    self.$pred_Submit = self.$el.find('#pred_Submit');
    self.$P1driverData = [];
    self.$predMM = self.$el.find('#pred_MM');
    self.$predSS = self.$el.find('#pred_SS');
    self.$predCC = self.$el.find('#pred_CC');
    self.$predDNF = self.$el.find('#pred_DNF');
    self.$pred_Rain = self.$el.find('#pred_Rain');
    self.$pred_Safety = self.$el.find('#pred_Safety');
    self.$pluginPoleTime = self.$pluginForm.find('#motorracingleague_pole_lap_time');
    self.$pluginDNF = self.$pluginForm.find('#motorracingleague_dnf');
    self.$pluginRain = self.$pluginForm.find('#motorracingleague_rain');
    self.$pluginSafety = self.$pluginForm.find('#motorracingleague_safety_car');
    self.$otherPredictionInputs = self.$el.find('#pred_MM, #pred_SS, #pred_CC, #pred_DNF, #pred_Rain, #pred_Safety');
    self.$tempCC;
    self.$tempMM;
    self.$tempSS;

    // preload driver images
    /*for(var i=0;i<self.$drivers.length;i++) {
        var el = self.$drivers[i];
        var thumb = new Image();
        thumb.src = window.siteUrl+'/wp-content/themes/purepitwall/assets/predictor/'+self.$drivers[i].driverThumb;
        var fullPic = new Image();
        fullPic.src = window.siteUrl+'/wp-content/themes/purepitwall/assets/predictor/'+self.$drivers[i].driverPicture;
    }*/

    // preload race calendar images
    for(var i=0;i<self.$races.length;i++) {
        var el = self.$races[i];
        var flag = new Image();
        flag.src = window.siteUrl+'/wp-content/themes/purepitwall/assets/predictor/'+self.$races[i].raceFlag;
        var map = new Image();
        map.src = window.siteUrl+'/wp-content/themes/purepitwall/assets/predictor/'+self.$races[i].raceMap;
    }


    // race select
    self.$isSelectOpen = false;
    self.$raceDisplaySelect.find('.currentRace, .arrow').on('click',function(){
        if (!self.$isSelectOpen) {
            self.openDropDown();
        } else {
            self.closeDropDown();
        }
    })

    // race select leaderboard
    self.$isLeaderboardSelectOpen = false;
    self.$leaderboardDisplaySelect.find('.currentRace, .arrow').on('click',function(){
        if (!self.$isLeaderboardSelectOpen) {
            self.openDropDownLeaderBoard();
        } else {
            self.closeDropDownLeaderBoard();
        }
    })

    // hide races elect if clicked outside
    $(document).click(function(event) {
        if(!$(event.target).closest(self.$raceDisplaySelect).length &&
           !$(event.target).is(self.$raceDisplaySelect)) {
            if (self.$isSelectOpen) {
                self.closeDropDown();
            }
        }

        if(!$(event.target).closest(self.$leaderboardDisplaySelect).length &&
           !$(event.target).is(self.$leaderboardDisplaySelect)) {
            if (self.$isLeaderboardSelectOpen) {
                self.closeDropDownLeaderBoard();
            }
        }
    })

    // position mode switch
    self.$positionSelect.on('click',function(){
        self.selectPosition($(this));
    })


    // other predictions
    self.$otherPredictionInputs.on('blur click',function(){
        self.setOtherPredictions();
    })

    // init
    if (self.$el.hasClass('logged-in')){
        self.buildRaceDropDown();
    } else {
        self.selectDriver('Lewis Hamilton');
        self.$mainPredictorInterface.on('click',function(){
            var newPos1 = self.$el.find('.pageContent.top').offset().top;
            TweenMax.to($(window), 1, {scrollTo:{y:newPos1,offsetY:70},overwrite:true,force3D:true,ease:Strong.easeInOut});
        });
    }

    // build driver select
    self.buildDriverSelect();

    // build leaderboard select
    self.buildLeaderBoardDropDown();


    // submit prediction
    var submitting = false;
    self.$pred_Submit.on('click',function(){
        self.$pluginSubmit.submit();
        submitting = true;
    })

    self.$messageAlert = self.$el.find('.messageAlert');
    self.$messageContent = self.$el.find('.msgContent');
    // watch for response for notice and update of submit button
    self.$pluginFormNotice.bind('DOMSubtreeModified', function() {
        // update submit button
        TweenMax.delayedCall(0.1,function(){
            self.$pred_Submit.val(self.$pluginSubmit.val());
        })
        if (submitting) {
            // Display Notice
            TweenMax.to(self.$messageAlert,0.5,{height:self.$messageContent.outerHeight(),ease:Strong.easeOut,overwrite:true,onComplete:function(){
                    TweenMax.to(self.$messageAlert,0.5,{height:0,ease:Strong.easeOut,overwrite:true,delay:3});
            }})
            submitting = false;
            // refresh page to update data on leaderboards
            TweenMax.delayedCall(1,function(){
                window.location.reload();
            })
        }

    });

    //?profiletab=main&um_action=edit

    // clean url
    if(document.location.search.length) {
        if (!window.location.href.indexOf("profiletab=main&um_action=edit=") > -1){
            //window.location = window.siteUrl+'/podiumpredictor';
        }
    }
    //history.pushState(null, "", location.href.split("?")[0]);


    // plus minus buttons
    var timeout;
    self.$plusMinusButtons = self.$el.find('.plusMinus_but');

    self.$plusMinusButtons.on('click',function(){
        //self.plusMinus($(this));
    });

    function mouseDown($target){
        timeout = setInterval( function(){
            self.plusMinus($target);
            return false;
        },70)

    }

    function mouseUp($target){
        clearInterval(timeout);
        self.setOtherPredictions();
        return false;
    }

    self.$plusMinusButtons.on('mousedown touchstart',function(){
        mouseDown($(this));
    });

    self.$plusMinusButtons.on('mouseup touchend',function(){
        mouseUp($(this));
    });

    self.$plusMinusButtons.mouseout(function(){
        mouseUp($(this));
    });

}

PodiumPredictor.prototype.plusMinus = function($target) {
    var self = this;
    var type;
    var metric;
    var inputTarget;

    if ($target.hasClass('plusMinus_plus')){
        type = 'plus';
    } else {
        type = 'minus';
    }


    if ($target.parent().hasClass('plusMinus_MM')) {
        inputTarget = $('#pred_MM');
        metric = 'minute';
    } else if ($target.parent().hasClass('plusMinus_SS')) {
        inputTarget = $('#pred_SS');
        metric = 'second';
    } else if ($target.parent().hasClass('plusMinus_CC')) {
        inputTarget = $('#pred_CC');
        metric = 'millisecond';
    } else {
        inputTarget = $('#pred_DNF');
        metric = 'dnf';
    }

    var newVal = parseInt(inputTarget.val());

    if (type === 'plus') {
        if (metric === 'minute') {
            if (newVal < 10) {
                inputTarget.val(newVal+1);
            } else {
                inputTarget.val(10);
            }
        } else if (metric === 'second') {
            if (newVal < 59) {
                inputTarget.val(newVal+1);
            } else {
                inputTarget.val(59);
            }
        } else if (metric === 'millisecond') {
            if (newVal < 999) {
                inputTarget.val(newVal+1);
            } else {
                inputTarget.val(999);
            }
        } else {
            if (newVal < 20) {
                inputTarget.val(newVal+1);
            } else {
                inputTarget.val(20);
            }
        }
    } else {
        if (newVal > 0) {
            inputTarget.val(newVal-1);
        }
    }

}

PodiumPredictor.prototype.setOtherPredictions = function(){
    var self = this;

    // limit inputs
    if (self.$predDNF.val() > 20) {
        self.$predDNF.val(20)
    }
    if (self.$predCC.val() > 999) {
        self.$predCC.val(999)
    }
    if (self.$predSS.val() > 59) {
        self.$predSS.val(59)
    }
    if (self.$predMM.val() > 10) {
        self.$predMM.val(10)
    }

    if (self.$predSS.val() < 10 && self.$predSS.val().length < 2){
        self.$predSS.val("0"+self.$predSS.val())
    }

    if (self.$predMM.val() < 10 && self.$predMM.val().length < 2){
        self.$predMM.val("0"+self.$predMM.val())
    }

    if (self.$predCC.val() < 100 && self.$predCC.val().length < 3){
        if (self.$predCC.val() < 10 && self.$predCC.val().length < 2){
            self.$predCC.val("00"+self.$predCC.val())
        } else {
            self.$predCC.val("0"+self.$predCC.val())
        }
    }

    // update pole time in plugin
    self.$pluginPoleTime.val(self.$predMM.val()+':'+self.$predSS.val()+'.'+self.$predCC.val());

    self.$tempMM = self.$predMM.val();
    self.$tempSS = self.$predSS.val();
    self.$tempCC = self.$predCC.val();

    // update dnf
    self.$pluginDNF.find("option[value="+self.$predDNF.val()+"]").prop("selected", true);
    self.$pluginDNF.trigger('change');
    // update checkboxes
    if (self.$pred_Rain.prop('checked') == true) {
        self.$pluginRain.prop('checked',true)
    } else {
        self.$pluginRain.prop('checked',false)
    }

    if (self.$pred_Safety.prop('checked') == true) {
        self.$pluginSafety.prop('checked',true)
    } else {
        self.$pluginSafety.prop('checked',false)
    }

    // check for submit
    self.checkForSubmit();
}

PodiumPredictor.prototype.selectPosition = function($el){
    var self = this;

    if (!$el.hasClass('selected')){
        if ($el.hasClass('position-1')){
            self.$currrentSelectPosition = self.$driverPos1;
            self.processUnavaible();
        }

        if ($el.hasClass('position-2')){
            self.$currrentSelectPosition = self.$driverPos2;
            self.processUnavaible();
        }

        if ($el.hasClass('position-3')){
            self.$currrentSelectPosition = self.$driverPos3;
            self.processUnavaible();
        }

        if ($el.hasClass('qualiPole')){
            self.$currrentSelectPosition = self.$driverQualiPole;
            self.$driverOptionsHolder.find('.driverOption').removeClass('unavailable');
        }

        if (self.$currrentSelectPosition.attr('data-driver') !== ''){
            self.selectDriver(self.$currrentSelectPosition.attr('data-driver'));
        }

        // mark selected
        var driver = self.$currrentSelectPosition.attr('data-driver');
        var $driverOptions = self.$driverOptionsHolder.find('.driverOption');
        $driverOptions.removeClass('selected');
        self.$driverOptionsHolder.find("[data-driver='" + driver + "']").removeClass('unavailable').addClass('selected');

        self.$positionSelect.removeClass('selected');
        $el.addClass('selected');
        var newPos2 = self.$mainPredictorInterface.offset().top;
        TweenMax.to($(window), 1, {scrollTo:{y:newPos2,offsetY:70},overwrite:true,force3D:true,ease:Strong.easeInOut});

        // check for submit
        self.checkForSubmit();
    }
}


PodiumPredictor.prototype.processDriverSelect = function($driver) {
    var self = this;
    // update selector view
    self.$currrentSelectPosition.attr('data-driver',$driver);
    self.$currrentSelectPosition.find('.positionAvatar').attr('class','positionAvatar '+$driver.replace(/\s/g,''));

    // update race selection on plugin
    self.$positionSelect.each(function(){
        var $driver = $(this).attr('data-driver');
        if ($(this).hasClass('position-1')){
            if ($driver !== ''){
                self.$pluginDriverP1.find('option:contains('+$driver+')').prop('selected', true);
                self.$pluginDriverP1.trigger('change');
            }
        }
        if ($(this).hasClass('position-2')){
            if ($driver !== ''){
                self.$pluginDriverP2.find('option:contains('+$driver+')').prop('selected', true);
                self.$pluginDriverP2.trigger('change');
            }
        }
        if ($(this).hasClass('position-3')){
            if ($driver !== ''){
                self.$pluginDriverP3.find('option:contains('+$driver+')').prop('selected', true);
                self.$pluginDriverP3.trigger('change');
            }
        }
        if ($(this).hasClass('qualiPole')){
            if ($driver !== ''){
                self.$pluginDriverPole.find('option:contains('+$driver+')').prop('selected', true);
                self.$pluginDriverPole.trigger('change');
            }
        }
    })
    // update other predictions
    self.setOtherPredictions();
    // check for submit
    self.checkForSubmit();
}


PodiumPredictor.prototype.processUnavaible = function(){
    var self = this;
    self.$driverOptionsHolder.find('.driverOption').removeClass('unavailable');
    self.$podiumPlaces.each(function(){
        var driver = $(this).attr('data-driver');
        if (driver !== ''){
            var target = self.$driverOptionsHolder.find("[data-driver='" + driver + "']");
            target.addClass('unavailable');
        }
    })
}

PodiumPredictor.prototype.buildDriverSelect = function() {
    var self = this;

    // build selector
    for(var i=0;i<self.$drivers.length;i++){
        self.$driverOptionsHolder.append('<div class="driverOption" data-driver="'+self.$drivers[i].driverName+'"><img src="'+window.siteUrl+'/wp-content/themes/purepitwall/assets/predictor/'+self.$drivers[i].driverNumLogo+'"></div>')
    }

    var $driverOptions = self.$driverOptionsHolder.find('.driverOption');

    // append events to options
    $driverOptions.on('click',function(){
        if (!$(this).hasClass('selected')){
            var selectedDriver = $(this).attr('data-driver');
            $driverOptions.removeClass('selected');
            $(this).addClass('selected');
            self.selectDriver(selectedDriver);

        }
    })
}

PodiumPredictor.prototype.selectDriver = function($driver){
    var self = this;

    // grab current driver data
    self.$lastSelectedDriver = self.$drivers.filter(function ( obj ) {
        return obj.driverName === $driver;
    })[0];


    // update display
    self.$driverPicture.html('<div class="driverDisplay" style="background-image: url('+window.siteUrl+'/wp-content/themes/purepitwall/assets/predictor/'+self.$lastSelectedDriver.driverNumLogo+'">').css('border-color',self.$lastSelectedDriver.driverColour);
    self.$driverName.html(self.$lastSelectedDriver.driverName);
    self.$driverTeam.html(self.$lastSelectedDriver.driverTeam);
    self.$driverDOB.html(self.$lastSelectedDriver.driverDOB);
    //self.$driverCarNumber.html(self.$lastSelectedDriver.driverCarNumber);
    self.$driverNationality.html(self.$lastSelectedDriver.driverNationality);
    self.$driverHeight.html(self.$lastSelectedDriver.driverHeight);
    self.$driverFirstGP.html(self.$lastSelectedDriver.driverFirstGP);

    // fade display in
    TweenMax.fromTo(self.$driverInfo,1,{alpha:0},{alpha:1,ease:Strong.easeOut});

    // process driver selection
    if (self.$el.hasClass('logged-in')){
        self.processDriverSelect($driver);
        // slide to top of select
        var newPos3 = self.$predictorOptionsHolder.offset().top;
        TweenMax.to($(window), 1, {scrollTo:{y:newPos3,offsetY:70},overwrite:true,force3D:true,ease:Strong.easeInOut});
    }

}

PodiumPredictor.prototype.openDropDown = function(){
    var self = this;
    self.$isSelectOpen = true;
    TweenMax.to(self.$raceDropDown,0.8,{className:'+=active',ease:Strong.easeInOut,force3D:true,overwrite:true});
    TweenMax.to(self.$arrowIcon,0.2,{rotation:-180,ease:Strong.easeout,force3D:true,overwrite:true});
}

PodiumPredictor.prototype.closeDropDown = function(){
    var self = this;
    TweenMax.to(self.$raceDropDown,0.6,{className:'-=active',ease:Strong.easeOut,force3D:true,overwrite:true});
    TweenMax.to(self.$arrowIcon,0.2,{rotation:0,ease:Strong.easeout,force3D:true,overwrite:true});
    self.$isSelectOpen = false;
}

PodiumPredictor.prototype.buildRaceDropDown = function(){
    var self = this;

    self.$raceOptions = self.$pluginRaces.find('option');

    // build options
    self.$raceOptions.each(function(){
        self.$raceDropDown.append('<div class="option"><div class="optionFlag '+$(this).text().replace(/\s/g,'')+'"></div>'+$(this).text()+'</div>')
    })

    // append events to options
    var raceDropDownOptions = self.$raceDropDown.find('.option');
    raceDropDownOptions.on('click',function(){
        var race = $(this).text();
        raceDropDownOptions.removeClass('selected');
        $(this).addClass('selected');
        self.selectRace(race);
        self.closeDropDown();
        // slide to top of select
        var newPos4 = self.$raceDisplaySelect.offset().top;
        TweenMax.to($(window), 1, {scrollTo:{y:newPos4,offsetY:70},overwrite:true,force3D:true,ease:Strong.easeInOut});
    })

    // select first race
    self.selectRace(self.$raceDropDown.find('.option').first().text());
    self.$raceDropDown.find('.option').first().addClass('selected');
}

PodiumPredictor.prototype.selectRace = function($race){
    var self = this;
    var selectedRace = $race;

    // update dropdown
    self.$racesSelectCurrent.find('.currentRaceName').html($race);
    self.$racesSelectCurrent.find('.optionFlag').attr('class','optionFlag '+$race.replace(/\s/g,''));

    // update race selection on plugin
    self.$pluginRaces.find('option:contains('+$race+')').prop('selected', true)
    self.$pluginRaces.trigger('change');

    // grab currentRace data
    self.$currentRace = self.$races.filter(function ( obj ) {
        return obj.raceShortName === selectedRace;
    })[0];

    // set data on page
    self.$raceDisplayName.html(self.$currentRace.raceName);
    self.$raceDisplayWeekend.html(self.$currentRace.raceWeekend);
    self.$raceDisplayCircuit.html(self.$currentRace.raceCircuit);
    self.$raceDisplayLocation.html(self.$currentRace.raceLocation);
    self.$lastYearPoleTime.html(self.$currentRace.lastYearPoleTime);
    self.$raceDisplayMap.attr('style','background-image:url('+window.siteUrl+'/wp-content/themes/purepitwall/assets/predictor/'+self.$currentRace.raceMap+')');


    // countdown
    self.selectedRaceCountdown(self.$currentRace);

    // fadeout driver info
    TweenMax.to(self.$driverInfo,1,{alpha:0,ease:Strong.easeOut,delay:1});

    // fade display in
    TweenMax.fromTo(self.$raceData,1,{alpha:0},{alpha:1,ease:Strong.easeOut,delay:0.6});

    // update predictor
    self.$currrentSelectPosition = self.$mainPredictorInterface.find('.position-1');
    self.$positionSelect.removeClass('selected');
    self.$el.find('.position-1').addClass('selected');

    // grab new race data
    TweenMax.delayedCall(1.25,function(){
        self.updatePredictor();
        // call it again, just in case!
        TweenMax.delayedCall(1,function(){
            self.updatePredictor();
        })
    })
}

PodiumPredictor.prototype.selectedRaceCountdown = function($race){
    var self = this;
    var now = new Date();
    self.$currentRaceTime = new Date($race.predictionDate);
    //self.userTimezoneOffset = self.$currentRaceTime.getTimezoneOffset() * 30000;
    self.$timeToCalc = new Date(self.$currentRaceTime.getTime() + self.userTimezoneOffset);
    self.$timeToCalc = new Date(self.$currentRaceTime.getTime());


    // start countdown
    self.$raceDisplayTime.countdown('destroy'); // reset
    self.$raceDisplayTime.countdown({until: self.$timeToCalc, padZeroes: true,labels:['Years', 'Months', 'Weeks', 'DAYS', 'HRS', 'MINS', 'SECS'],labels1:['Year', 'Month', 'Week', 'DAY', 'HRS', 'MINS', 'SECS']});
}


PodiumPredictor.prototype.updatePredictor = function(){
    // grabs data from racing plugin, then displays it
    var self = this;

    var positions = [self.$driverQualiPole, self.$driverPos1, self.$driverPos2, self.$driverPos3];
    var pluginPositions = [self.$pluginDriverPole,self.$pluginDriverP1,self.$pluginDriverP2,self.$pluginDriverP3];

    for (var i=0;i<positions.length;i++){
        var el = positions[i];
        var $driver = pluginPositions[i].find(":selected").text();

        el.attr('data-driver',$driver);
        el.find('.positionAvatar').attr('class','positionAvatar '+$driver.replace(/\s/g,''));
    }

    // update other predictions
    // pole time
    if (self.$pluginPoleTime.val()){
        var arr = self.$pluginPoleTime.val().split(':');
        var arr2 = arr[1].split('.');
        self.$predMM.val(arr[0]);
        self.$tempMM = arr[0];
        self.$predSS.val(arr2[0]);
        self.$tempSS = arr2[0];
        self.$predCC.val(arr2[1]);
        self.$tempCC = arr2[1];
    } else {
        self.$predMM.val("00");
        self.$predSS.val("00");
        self.$predCC.val("000");
        self.$tempMM = "00";
        self.$tempSS = "00";
        self.$tempCC = "000";
    }

    // dnf
    self.$predDNF.val(self.$pluginDNF.val());
    // rain
    if (self.$pluginRain.prop('checked') == true) {
        self.$pred_Rain.prop('checked',true)
    } else {
        self.$pred_Rain.prop('checked',false)
    }
    // safety car
    if (self.$pluginSafety.prop('checked') == true) {
        self.$pred_Safety.prop('checked',true)
    } else {
        self.$pred_Safety.prop('checked',false)
    }

    // get driver data for P1
    self.$P1driverData = self.$drivers.filter(function ( obj ) {
        return obj.driverName === positions[1].attr('data-driver');
    })[0];

    var $driverOptions = self.$driverOptionsHolder.find('.driverOption');

    // update main driver display with P1 prediction
    if (positions[1].attr('data-driver') !== ''){
        self.$driverPicture.html('<div class="driverDisplay" style="background-image: url('+window.siteUrl+'/wp-content/themes/purepitwall/assets/predictor/'+self.$P1driverData.driverNumLogo+'">').css('border-color',self.$lastSelectedDriver.driverColour);
        self.$driverName.html(self.$P1driverData.driverName);
        self.$driverTeam.html(self.$P1driverData.driverTeam);
        self.$driverDOB.html(self.$P1driverData.driverDOB);
        //self.$driverCarNumber.html(self.$P1driverData.driverCarNumber);
        self.$driverNationality.html(self.$P1driverData.driverNationality);
        self.$driverHeight.html(self.$P1driverData.driverHeight);
        self.$driverFirstGP.html(self.$P1driverData.driverFirstGP);

        // fade display in
        TweenMax.to(self.$driverInfo,1,{alpha:1,ease:Strong.easeOut});

        // process driver selection
        self.processUnavaible();
        $driverOptions.removeClass('selected');
        self.$driverOptionsHolder.find("[data-driver='" + self.$P1driverData.driverName + "']").removeClass('unavailable').addClass('selected');
    } else {
        // fade display out
        TweenMax.to(self.$driverInfo,1,{alpha:0,ease:Strong.easeOut});
        $driverOptions.removeClass('selected').removeClass('unavailable');
    }

    // update submit button
    self.checkForSubmit();
    self.$pred_Submit.val(self.$pluginSubmit.val());
}


PodiumPredictor.prototype.buildLeaderBoardDropDown = function(){
    var self = this;

    // add overall option
    self.$leaderboardDropDown.append('<div class="option"><div class="optionFlag Overall"></div>Overall</div>');

    // build selector
    for(var i=0;i<self.$races.length;i++){
        self.$leaderboardDropDown.append('<div class="option"><div class="optionFlag '+self.$races[i].raceShortName.replace(/\s/g,'')+'"></div>'+self.$races[i].raceShortName+'</div>')

        // decide stat display, destroy postDeadLine data
        var contextRace = self.$statsContainer.find('.'+self.$races[i].raceShortName.replace(/\s/g,''));
        var preDeadlineStat = contextRace.find('.preDeadline');
        var postDeadlineStat = contextRace.find('.postDeadline');

        var now = new Date();
        var contextRaceDeadline = new Date(self.$races[i].predictionDate);

        if (now < contextRaceDeadline) {
            preDeadlineStat.css('display','block');
            postDeadlineStat.css('display','none');
            postDeadlineStat.html('');
        } else {
            postDeadlineStat.css('display','block');
            preDeadlineStat.css('display','none');
            preDeadlineStat.html('');
        }

        // hide leadeboard before first race
        if (i === 0) {
            if (now < new Date(self.$races[0].raceDate)) {
                self.$el.find('.mainLeaderBoardPanel').css('display', 'none');
                // remove page bottom padding
                if (!self.$el.hasClass('logged-in')){
                    self.$el.find('.pageContent.bottom').css('padding',0);
                }
            }
        }
    }

    // append events to options
    var raceDropDownOptions = self.$leaderboardDropDown.find('.option');
    raceDropDownOptions.on('click',function(){
        var race = $(this).text();
        raceDropDownOptions.removeClass('selected');
        $(this).addClass('selected');
        self.selectRaceLeaderBoard(race);
        self.closeDropDownLeaderBoard();
    })

    // select first race
    self.selectRaceLeaderBoard(self.$leaderboardDropDown.find('.option').first().text());
    self.$leaderboardDropDown.find('.option').first().addClass('selected');


    // resize
    $(window).on('resize',function(){
        TweenMax.to(self.$statsContainer,1,{height:self.$statsContainer.find('selected').outerHeight(),ease:Strong.easeInOut,overwrite:true});
    })
}

PodiumPredictor.prototype.openDropDownLeaderBoard = function(){
    var self = this;
    self.$isLeaderboardSelectOpen = true;
    TweenMax.to(self.$leaderboardDropDown,0.8,{className:'+=active',ease:Strong.easeInOut,force3D:true,overwrite:true});
    TweenMax.to(self.$arrowIconLeadeboard,0.2,{rotation:-180,ease:Strong.easeout,force3D:true,overwrite:true});
}

PodiumPredictor.prototype.closeDropDownLeaderBoard = function(){
    var self = this;
    TweenMax.to(self.$leaderboardDropDown,0.6,{className:'-=active',ease:Strong.easeOut,force3D:true,overwrite:true});
    TweenMax.to(self.$arrowIconLeadeboard,0.2,{rotation:0,ease:Strong.easeout,force3D:true,overwrite:true});
    self.$isLeaderboardSelectOpen = false;
}

PodiumPredictor.prototype.selectRaceLeaderBoard = function($race){
    var self = this;
    var selectedRace = $race;

    // update dropdown
    self.$leaderboardSelectCurrent.find('.currentRaceName').html($race);
    self.$leaderboardSelectCurrent.find('.optionFlag').attr('class','optionFlag '+$race.replace(/\s/g,''));


    var oldSelected = self.$statsContainer.find('.selected');
    self.$statsContainer.css('height',oldSelected.outerHeight());

    // get selected item
    var newSelected = self.$statsContainer.find('.'+$race.replace(/\s/g,''));
    var selectedHeight = newSelected.outerHeight();


    // fadeout current item
    TweenMax.to(oldSelected,0.5,{alpha:0,force3D:true,ease:Strong.easeOut,onComplete:function(){
        oldSelected.removeClass('selected');
        // animate stats container height
        TweenMax.set(newSelected,{alpha:0});
        TweenMax.to(self.$statsContainer,0.5,{height:selectedHeight,ease:Strong.easeInOut,force3D:true,overwrite:true,onComplete:function(){
            TweenMax.to(newSelected,0.5,{alpha:1,force3D:true,ease:Strong.easeOut});
            newSelected.addClass('selected');
        }})
    }});
}

PodiumPredictor.prototype.checkForSubmit = function(){
    var self = this;
    if (self.$driverPos1.attr('data-driver') !== '' && self.$driverPos2.attr('data-driver') !== '' && self.$driverPos3.attr('data-driver') !== '' && self.$driverQualiPole.attr('data-driver') !== '' && self.$predCC.val().length === 3 && self.$predMM.val().length === 2 && self.$predSS.val().length === 2 ) {
        self.$pred_Submit.removeClass('disabled')
    } else {
        self.$pred_Submit.addClass('disabled')
    }
}


module.exports = PodiumPredictor;
