'use strict';
var SitePage = function($target){
    // set the element
    var self = this;
    self.$el = $target;
    self.$pageHeroContent = self.$el.find('.pageHeroContent');
    self.$pageTitle = self.$el.find('.pageHeroContent h1');

    // split word, style second word
    self.$pageTitle.html(function(i, v) {
        return v.replace(/(\s)(\w+)/, '$1<span class="secondWord">$2</span>');
    });


    // slide pageHeroContent
    TweenMax.fromTo(self.$pageHeroContent,0.75,{alpha:0,y:100},{alpha:1,y:0,ease:Strong.easeOut,force3D:true,overwrite:true,delay:0.5,force3D:true});


};

module.exports = SitePage;
