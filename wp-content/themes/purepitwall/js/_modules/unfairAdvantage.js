'use strict';

var ParallaxController = require('../../js/utils/parallax');

var UnfairAdvantage = function($target){
    var self = this;
    self.$el = $target;

    // video background
    self.$video = self.$el.find('.bg-video');
    self.$videoUrl = self.$video.attr('data-url');
    self.$videoBg = self.$el.find('.videoBg');
    if (isMobile.any){
      self.$video.hide();
    } else {
      self.$videoBg.css('background-image','none');
    }
    self.$video.get(0).volume = 0;
    self.$backgroundVideo = new BackgroundVideo('.bg-video', {
      src: [self.$videoUrl]
    });

    // videos slider
    self.$swiperContainer = self.$el.find('.videosHolder .swiper-container');
    self.$swiperSlides = self.$el.find('.videosHolder .swiper-slide');

    if (self.$swiperSlides.length){
        var mySwiper = new Swiper(self.$swiperContainer, {
            direction: 'horizontal',
            speed: 500,
            loop: false,
            pagination: {
                el: '.videosHolder .pagination',
                clickable: true,
            },
            autoHeight: true,
            navigation: {
                nextEl: '.videosHolder .nextArrow',
                prevEl: '.videosHolder .prevArrow',
            },
            grabCursor: true,
            centeredSlides: true,
            slidesPerView: 'auto',
            effect: 'coverflow',
            coverflow: {
              rotate: 40,
              stretch: 0,
              depth: 300,
              modifier: 0.75,
              slideShadows : true
            },
            onSlideChangeEnd : function(swiper){
                //self.changeBackground();
            }
        });
    } else {
        // hide how it works if there's no videos
        self.$el.find('.ua_videos').hide();
    }

    // case studies slider
    self.$caseStudyContainer = self.$el.find('.ua_slides .swiper-container');
    self.$caseStudySlides = self.$el.find('.ua_slides .swiper-slide');

    if (self.$caseStudySlides.length){
        var mySwiper = new Swiper(self.$caseStudyContainer, {
            direction: 'horizontal',
            speed: 500,
            loop: true,
            pagination: {
                el: '.ua_slides .pagination',
                clickable: true,
            },
            autoHeight: true,
            navigation: {
                nextEl: '.ua_slides .nextArrow',
                prevEl: '.ua_slides .prevArrow',
            },
            grabCursor: true,
            centeredSlides: true,
            slidesPerView: 'auto',
            effect: 'coverflow',
            coverflow: {
              rotate: 40,
              stretch: 0,
              depth: 300,
              modifier: 0.75,
              slideShadows : true
            },
            onSlideChangeEnd : function(swiper){
                //self.changeBackground();
            }
        });
    }

    // parallax units
    self.$isParallax = self.$el.find('.ua_module.is-parallax');
    self.$characterParllax = self.$el.find('.leftChar.is-parallax, .rightChar.is-parallax');
    self.$isComponentsAnimating = false;
    self.$isProductsAnimating = false;
    self.$isStorageAnimating = false;
    self.$isReductionAnimating = false;
    self.$isStopWatchesAnimating = false;
    self.$isHoursAnimating = false;
    self.$isMinutesAnimating = false;
    self.$isLeftCharAnimating = false;
    self.$isRightCharAnimating = false;

    self.$isParallax.on('parallax.scroll', function(e, data) {
        var scrollAmount = data.position.toFixed(2);
        scrollAmount = scrollAmount < 0 ? 0.01 : scrollAmount;
        if (scrollAmount < 1 && scrollAmount > 0.35) {

            switch (true) {
                case $(this).hasClass('ua_products'):
                    self.animateChips($(this));
                    self.animateComponents($(this));
                break;

                case $(this).hasClass('ua_storage'):
                    self.animateStorage($(this));
                break;

                case $(this).hasClass('ua_reduction'):
                    self.animateReduction($(this));
                break;

                case $(this).hasClass('ua_hours'):
                    self.animateStopWatches($(this));
                    self.animateHours($(this));
                break;

                case $(this).hasClass('ua_minutes'):
                    self.animateMinutes($(this));
                break;
            }
        }
    });

    self.$characterParllax.on('parallax.scroll', function(e, data) {
        var scrollAmount = data.position.toFixed(2);
        scrollAmount = scrollAmount < 0 ? 0.01 : scrollAmount;
        if (scrollAmount < 1 && scrollAmount > 0.30) {

            switch (true) {

                case $(this).hasClass('leftChar'):
                    self.animateCharacters($(this),'left');
                break;

                case $(this).hasClass('rightChar'):
                    self.animateCharacters($(this),'right');
                break;
            }
        }
    });

    // fade in first module
    TweenMax.to(self.$el.find('.ua_chips .container-fluid'),0.5,{alpha:1,delay:1});
};

UnfairAdvantage.prototype.animateStopWatches = function($el) {
    var self = this;
    if (self.$isStopWatchesAnimating === false) {
        self.$isStopWatchesAnimating = true;
        $el.find('.stopwatch').each(function(i){
            var el = $(this);
            TweenMax.fromTo(el,18,{rotation:-5, y:-10},{rotation:5,y:0,yoyo:true,repeat:-1,ease:Strong.easeInOut,overwrite:false});
            TweenMax.fromTo(el,2,{alpha:0},{alpha:1,ease:Strong.easeOut,delay:1+(i*0.4),overwrite:false});
        });
    }
};

UnfairAdvantage.prototype.animateComponents = function($el) {
    var self = this;
    if (self.$isComponentsAnimating === false) {
        self.$isComponentsAnimating = true;
        $el.find('.sensor').each(function(i){
            var el = $(this);
            TweenMax.fromTo(el,18,{rotation:-10},{rotation:10,yoyo:true,repeat:-1,ease:Strong.easeInOut,overwrite:false});
            TweenMax.fromTo(el,2,{alpha:0},{alpha:1,ease:Strong.easeOut,delay:1+(i*0.4),overwrite:false});
        });
    }
};

UnfairAdvantage.prototype.animateChips = function($el) {
    var self = this;
    if (self.$isProductsAnimating === false) {
        self.$isProductsAnimating = true;
        TweenMax.to($el.find('.chipsHolder'),1,{alpha:1,onComplete:function(){
            $el.find('.stats-counter').each(function(){
                var el = $(this);
                self.countUp(el);
            });
            //TweenMax.to($el.find('.chipsBgMask'),6,{className:'+=active',ease:Strong.eseOut});
            TweenMax.to($el.find('.capture'),1,{alpha:1,delay:2});
        }});
    }
};

UnfairAdvantage.prototype.animateStorage = function($el) {
    var self = this;
    if (self.$isStorageAnimating === false) {
        self.$isStorageAnimating = true;
        TweenMax.to($el.find('.smallBar'),1,{className:'+=active',ease:Strong.easeInOut,force3D:true});
        TweenMax.to($el.find('.bigBar'),1.25,{className:'+=active',ease:Strong.easeInOut,force3D:true,delay:0.75});
    }

};

UnfairAdvantage.prototype.animateReduction = function($el) {
    var self = this;
    if (self.$isReductionAnimating === false) {
        self.$isReductionAnimating = true;
        TweenMax.to($el.find('.container-fluid'),1,{alpha:1,onComplete:function(){
            $el.find('.stats-counter').each(function(){
                var el = $(this);
                self.countUp(el);
            });
            TweenMax.to($el.find('.graphLine'),2,{className:'+=active',ease:Strong.easeOut,force3D:true});
            TweenMax.to($el.find('.mNum25'),0.5,{alpha:1,delay:0.2});
            TweenMax.to($el.find('.mNum50'),0.5,{alpha:1,delay:0.5});
            TweenMax.fromTo($el.find('.mNum68'),1,{scale:0,alpha:0},{scale:1,alpha:1,ease:Back.easeOut,delay:1,force3D:true});
        }});
    }
};

UnfairAdvantage.prototype.animateHours = function($el) {
    var self = this;
    if (self.$isHoursAnimating === false) {
        self.$isHoursAnimating = true;
        TweenMax.to($el.find('.container-fluid'),1,{alpha:1,onComplete:function(){
            $el.find('.stats-counter').each(function(){
                var el = $(this);
                self.countUp(el);
            });
            TweenMax.to($el.find('.pie.left .fill'),0.7,{rotation:0,ease:Strong.easeIn,force3D:true,onComplete:function(){
                TweenMax.to($el.find('.pie.right .fill'),0.2,{rotation:0,ease:Linear.easeNone,force3D:true,onComplete:function(){
                    $el.find('.pieHolder').addClass('secondRun');
                    TweenMax.to($el.find('.pie.left .fill'),0.2,{rotation:-180,ease:Linear.easeNone,force3D:true,onComplete:function(){
                            TweenMax.to($el.find('.pie.right .fill'),0.7,{rotation:-180,ease:Strong.easeOut,force3D:true});

                            TweenMax.to($el.find('.pureLogo'),1.1,{alpha:1,delay:0.7,ease:Strong.easeOut});
                    }});
                }})
            }});
        }});
    }
};

UnfairAdvantage.prototype.animateMinutes = function($el) {
    var self = this;
    if (self.$isMinutesAnimating === false) {
        self.$isMinutesAnimating = true;
        TweenMax.to($el.find('.container-fluid'),1,{alpha:1,onComplete:function(){
            $el.find('.stats-counter').each(function(){
                var el = $(this);
                self.countUp(el);
            });
            TweenMax.to($el.find('.meterLevel'),1.5,{className:'+=active',ease:Strong.easeInOut,force3D:true});

        }});
    }
};

UnfairAdvantage.prototype.animateCharacters = function($el,direction) {
    var self = this;
    function animate(){
        TweenMax.fromTo($el.find('.charImg'),2,{y:50,alpha:0},{y:0,alpha:1,ease:Strong.easeOut,delay:0.5,force3D:true,onStart:function(){
                TweenMax.fromTo($el.find('.charSpeechBubble'),1,{y:50,alpha:0},{y:0,alpha:1,delay:0.5,ease:Strong.easeOut,force3D:true});
        }});
    }

    if (direction === 'left') {
        if (self.$isLeftCharAnimating === false) {
            self.$isLeftCharAnimating = true;
            animate();
        }
    } else {
        if (self.$isRightCharAnimating === false) {
            self.$isRightCharAnimating = true;
            animate();
        }
    }
}


UnfairAdvantage.prototype.commaSeparateNumber = function(val) {
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
};

UnfairAdvantage.prototype.countUp = function($el){
    var self = this;
    var countDuration = 1.5;
    var isCounting = false;
    if (isCounting === false) {
        isCounting = true;
        var el = $el;
        el.html('0');
        var startScore = {score:0}, scoreDisplay = el;
        var endScore = $el.attr('data-number');
        TweenMax.to(startScore, countDuration, {score:endScore, roundProps:"score", onUpdate:function(){
            var scoreFormatted = self.commaSeparateNumber(startScore.score);
            scoreDisplay.text(scoreFormatted);
        },onComplete:function(){
            //self.isNumbersCounting = false;
        },delay:0});
    }
};


module.exports = UnfairAdvantage;
