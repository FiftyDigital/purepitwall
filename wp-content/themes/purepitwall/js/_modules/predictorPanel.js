'use strict';

var PredictorPanel = function($target){
    var self = this;
    self.$el = $target;
    self.$toggleBar = self.$el.find('.panelToggleBar');
    self.isPanelOpen = true;
    self.$races = window.$races;
    self.$nextRace = [];
    self.$nextRaceTime;
    self.$nextRaceName;

    self.$toggleBar.on('click',function(e){
        e.preventDefault();
        e.stopPropagation();
        if(!self.isPanelOpen) {
            // open
            self.$el.addClass('panel_active');
            self.isPanelOpen = true;
        } else {
            // close
            self.$el.removeClass('panel_active');
            self.isPanelOpen = false;
        }
    })

    // predictor
    self.$races = window.$races;
    self.$drivers = window.$drivers;
    self.$currentRace = [];
    self.$currentRaceTime;
    self.$racingPlugin = self.$el.find('.racingPlugin');
    self.$predictionsTable = self.$el.find('.predictionsTable');
    self.$mainPredictorInterface = self.$el.find('.mainPredictorInterface');
    self.$podiumPlaces = self.$el.find('.position-1, .position-2, .position-3');
    self.$driverPos1 = self.$mainPredictorInterface.find('.js-position.position-1');
    self.$driverPos2 = self.$mainPredictorInterface.find('.js-position.position-2');
    self.$driverPos3 = self.$mainPredictorInterface.find('.js-position.position-3');
    self.$driverQualiPole = self.$mainPredictorInterface.find('.js-position.qualiPole');
    self.$predMM = self.$el.find('#pred_MM');
    self.$predSS = self.$el.find('#pred_SS');
    self.$predCC = self.$el.find('#pred_CC');
    self.$predDNF = self.$el.find('#pred_DNF');
    self.$pred_Rain = self.$el.find('#pred_Rain');
    self.$pred_Safety = self.$el.find('#pred_Safety');
    self.$otherPredictionInputs = self.$el.find('#pred_MM, #pred_SS, #pred_CC, #pred_DNF, #pred_Rain, #pred_Safety');
    self.$tempCC;
    self.$tempMM;
    self.$tempSS;


    // live social, notification
    if (!isMobile.apple.device) {
        var refreshMinutes = 6;
        self.$notificationBar = $('.notificationBar');
        self.$barContent = $('.barContent');
        self.$barClose = $('.barClose');
        // $('.livesSocialStream iframe').iFrameResize();
        $('.livesSocialStream iframe').css('display','block');
        $('.livesSocialStream .feedDirect').css('display','none');
        $('.livesSocialStream iframe').attr('src','/live-social-panel');

        function reloadIFrame() {
            $('.livesSocialStream iframe').attr('src','/live-social-panel');
            if (!self.isPanelOpen) {
                self.openBar()
            }
        }
        window.setInterval(function(){
            reloadIFrame()
        },refreshMinutes * 60000);

        self.$barClose.on('click',function(e) {
          self.closeBar()
        })

        self.$barContent.on('click',function() {
            if (!self.isPanelOpen) {
                // open
                self.$el.addClass('panel_active');
                self.isPanelOpen = true;
            }
            self.closeBar()
            var newPos1 = $('.panelToggleBar').offset().top;
            TweenMax.to($(window), 1, {scrollTo:{y:newPos1,offsetY:70},overwrite:true,force3D:true,ease:Strong.easeInOut});
        })
    }

    self.getNextRace();
}

PredictorPanel.prototype.openBar = function(){
    var self = this;
    TweenMax.to($('.notificationBar'),1,{className:'+=active',ease:Strong.easeOut,delay:3})
}

PredictorPanel.prototype.closeBar = function(){
    var self = this;
    TweenMax.to($('.notificationBar'),0.65,{className:'-=active',ease:Strong.easeOut})
}

PredictorPanel.prototype.getNextRace = function(){
    var self = this;
    var now = new Date();

    function newFindClosest(dates, testDate) {
        var before = [];
        var after = [];
        var max = dates.length;
        for(var i = 0; i < max; i++) {
            var tar = dates[i];
            var arrDate = new Date(tar.raceDate);
            // 3600 * 24 * 1000 = calculating milliseconds to days, for clarity.
            var diff = (arrDate - testDate) / (3600 * 24 * 1000);
            if(diff > 0) {
                before.push({diff: diff, index: i, raceShortName: tar.raceShortName, raceDate: tar.raceDate});
            } else {
                after.push({diff: diff, index: i, raceShortName: tar.raceShortName, raceDate: tar.raceDate});
            }
        }
        before.sort(function(a, b) {
            if(a.diff < b.diff) {
                return -1;
            }
            if(a.diff > b.diff) {
                return 1;
            }
            return 0;
        });

        after.sort(function(a, b) {
            if(a.diff > b.diff) {
                return -1;
            }
            if(a.diff < b.diff) {
                return 1;
            }
            return 0;
        });
        return {datesBefore: before, datesAfter: after};
    }

    self.$nextRace = newFindClosest(self.$races, new Date(now.getTime() + (-180 * 60 * 1000))); // 180 = 3 hours
    self.$nextRaceTime = new Date(self.$nextRace.datesBefore[0].raceDate);
    self.$nextRaceName = self.$nextRace.datesBefore[0].raceShortName;
    self.updatePredictor(self.$nextRaceName);
}

PredictorPanel.prototype.updatePredictor = function(currentRace){
    var self = this;
    // get current race prediction from table
    var currentRacePrediction = self.$predictionsTable.find('th:contains('+currentRace+')').parent();

    if (currentRacePrediction.length) {
        var rain = currentRacePrediction.find('td:nth-child(2) img').attr('alt')
        var SC = currentRacePrediction.find('td:nth-child(3) img').attr('alt')
        var DNF = currentRacePrediction.find('td:nth-child(4) span').html()
        var poleTime = currentRacePrediction.find('td:nth-child(5) span').html()
        var poleDriver = currentRacePrediction.find('td:nth-child(6) span').attr('title').replace(' +0','')
        var predPos1 = currentRacePrediction.find('td:nth-child(7) span').attr('title').replace(' +0','')
        var predPos2 = currentRacePrediction.find('td:nth-child(8) span').attr('title').replace(' +0','')
        var predPos3 = currentRacePrediction.find('td:nth-child(9) span').attr('title').replace(' +0','')

        // positions
        var positions = [self.$driverQualiPole, self.$driverPos1, self.$driverPos2, self.$driverPos3];
        var pluginPositions = [poleDriver,predPos1,predPos2,predPos3];
        for (var i=0;i<positions.length;i++){
            var el = positions[i];
            var $driver = pluginPositions[i];
            el.attr('data-driver',$driver);
            el.find('.positionAvatar').attr('class','positionAvatar '+$driver.replace(/\s/g,''));
        }
        // DNF
        self.$predDNF.val(DNF);
        // pole time
        if (poleTime){
            var arr = poleTime.split(':');
            var arr2 = arr[1].split('.');
            self.$predMM.val(arr[0]);
            self.$tempMM = arr[0];
            self.$predSS.val(arr2[0]);
            self.$tempSS = arr2[0];
            self.$predCC.val(arr2[1]);
            self.$tempCC = arr2[1];
        } else {
            self.$predMM.val("00");
            self.$predSS.val("00");
            self.$predCC.val("000");
            self.$tempMM = "00";
            self.$tempSS = "00";
            self.$tempCC = "000";
        }
        // rain
        if (rain === '✔') {
            self.$pred_Rain.prop('checked',true)
        } else {
            self.$pred_Rain.prop('checked',false)
        }
        // safety car
        if (SC === '✔') {
            self.$pred_Safety.prop('checked',true)
        } else {
            self.$pred_Safety.prop('checked',false)
        }
    }
}

module.exports = PredictorPanel;
