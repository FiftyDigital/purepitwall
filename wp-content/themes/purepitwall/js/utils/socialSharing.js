'use strict';

var SocialSharing = function($target){
    var self = this;
    self.$el = $target;



    if (self.$el.hasClass('js-socialSharing')){
        self.$el.jsSocials({
            shares: [
            "facebook",
            "twitter",
            "linkedin",
            "googleplus"],
            showLabel: false,
            showCount: false,
            shareIn: "popup"
        });
    }


    if (self.$el.is('blockquote')){
        var quote = self.$el.text();
        self.$el.append('<div class="js-socialSharing"></div>');
        var jsSs = self.$el.find('.js-socialSharing');
        jsSs.jsSocials({
            shares: [
            "facebook",
            "twitter",
            "linkedin",
            "googleplus"],
            showLabel: false,
            showCount: false,
            shareIn: "popup",
            text: quote
        });
    }
}

module.exports = SocialSharing;
