"use strict";

var ParallaxController = function($target) {
    var self = this;
    // set the el
    self.$el = $target;

    // bind to scroll
    $(document).on('window.scroll', lodashBind(self.setScroll, self));

};


ParallaxController.prototype.setScroll = function() {

    var self = this;
    var scrollTop = $(window).scrollTop();
    var offsetTop = self.$el.offset().top;
    var moduleHeight = self.$el.outerHeight();

    /*if (offsetTop > (scrollTop)) {
        return 0;
    } else if ((offsetTop + moduleHeight) < (scrollTop + window.h)) {
        return 100;
    } else {
        var distance = (scrollTop + window.h) - offsetTop;
        var percentage = distance / ((window.h) / 100);

        percentage = percentage/100 -1;
        // prevent percentage dropping below 0
        percentage = percentage <= 0 ? 0 : percentage;

        self.$el.trigger('parallax.scroll', [{
            el: self.$el,
            position: percentage
        }]);

    }*/

    if (offsetTop > (scrollTop + window.h)) {
        return 0;
    } else if ((offsetTop + moduleHeight) < scrollTop) {
        return 100;
    } else {
        var distance = (scrollTop + window.h) - offsetTop;
        var percentage = distance / ((window.h + moduleHeight) / 100);

        percentage = percentage/100;
        // prevent percentage dropping below 0
        percentage = percentage <= 0 ? 0 : percentage;

        self.$el.trigger('parallax.scroll', [{
            el: self.$el,
            position: percentage
        }]);
    }

};

module.exports = ParallaxController;
