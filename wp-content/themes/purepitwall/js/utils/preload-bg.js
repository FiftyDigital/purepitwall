'use strict';

var PreloadBg = function($target){
    var self = this;
    self.$el = $target;
    self.$heroImage = self.$el;
    self.heroImageFile = self.$heroImage.css('background-image');
    self.heroImageFile = self.heroImageFile.replace('url(','').replace(')','').replace(/\"/gi, "");

    //  preload background image, then fade in
    var bgImage = new Image();
    bgImage.onload = function() {
      TweenMax.fromTo(self.$heroImage,1.5,{alpha:0,scale:1.3},{scale:1,alpha:1,delay:0.15,ease:Strong.easeOut});
      if (self.$el.hasClass('__heroImage')){
        $(window).trigger('window.bgLoaded');
      }
    };
    bgImage.src = self.heroImageFile;

}

module.exports = PreloadBg;
