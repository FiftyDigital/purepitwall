'use strict';

var RaceCountDown = function($target){
    var self = this;
    self.$el = $target;
    //self.$racesLoaded = false;
    self.$races = window.$races;
    self.$nextRace = [];
    self.$nextRaceTime;
    self.$nextRaceName;
    self.$raceTime = self.$el.find('.raceTime');
    self.$raceName = self.$el.find('.raceName');
    // load races json
    //self.loadRaceData();
    self.getNextRace();
}

/*RaceCountDown.prototype.loadRaceData = function(){
    var self = this;
    $.getJSON("./wp-content/themes/purepitwall/js/2017raceCalendar.json", function(data) {
        $.each(data.races,function(key,val) {
            self.$races.push(val);
        });

    })
    .done(function() {
        self.$racesLoaded = true;
        self.getNextRace();
    })
    .fail(function() {
        self.loadRaceData();
    })
}*/

RaceCountDown.prototype.getNextRace = function(){
    var self = this;
    var now = new Date();

    //console.log(now)

    function newFindClosest(dates, testDate) {
        var before = [];
        var after = [];
        var max = dates.length;
        for(var i = 0; i < max; i++) {
            var tar = dates[i];
            var arrDate = new Date(tar.raceDate);
            // 3600 * 24 * 1000 = calculating milliseconds to days, for clarity.
            var diff = (arrDate - testDate) / (3600 * 24 * 1000);
            if(diff > 0) {
                before.push({diff: diff, index: i, raceName: tar.raceName, raceDate: tar.raceDate});
            } else {
                after.push({diff: diff, index: i, raceName: tar.raceName, raceDate: tar.raceDate});
            }
        }
        before.sort(function(a, b) {
            if(a.diff < b.diff) {
                return -1;
            }
            if(a.diff > b.diff) {
                return 1;
            }
            return 0;
        });

        after.sort(function(a, b) {
            if(a.diff > b.diff) {
                return -1;
            }
            if(a.diff < b.diff) {
                return 1;
            }
            return 0;
        });
        return {datesBefore: before, datesAfter: after};
    }


    self.$nextRace = newFindClosest(self.$races, now);
    self.$nextRaceTime = new Date(self.$nextRace.datesBefore[0].raceDate);
    //self.userTimezoneOffset = self.$nextRaceTime.getTimezoneOffset() * 30000;
    //self.$timeToCalc = new Date(self.$nextRaceTime.getTime() + self.userTimezoneOffset);
    self.$timeToCalc = new Date(self.$nextRaceTime.getTime());
    self.$nextRaceName = self.$nextRace.datesBefore[0].raceName;

    //console.log(self.$nextRaceName+', '+self.$nextRaceTime);

    // start countdown
    self.$raceTime.countdown({until: self.$timeToCalc, padZeroes: true,labels:['Years', 'Months', 'Weeks', 'DAYS', 'HRS', 'MINS', 'SECS'],labels1:['Year', 'Month', 'Week', 'DAY', 'HRS', 'MINS', 'SECS']});

    self.$raceName.html(self.$nextRaceName);
}

module.exports = RaceCountDown;
