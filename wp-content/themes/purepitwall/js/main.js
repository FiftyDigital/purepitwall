'use strict';
// IE listener fix
if (typeof(UserAgentInfo) != 'undefined' && !window.addEventListener){
    UserAgentInfo.strtBrowser = 1;
}
window.isIE = function() {
  var myNav = navigator.userAgent.toLowerCase();
  return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
}

//window.$ = require('jquery');
//window.jQuery = $;
window.$ = jQuery;
window.w = window.innerWidth;
window.h = window.innerHeight;
window.lodashBind = require('lodash.bind');
window.TweenMax = require('gsap/src/uncompressed/TweenMax');
window.ScrollTo = require('gsap/src/uncompressed/plugins/ScrollToPlugin');
window.Draggable = require ('gsap/src/uncompressed/utils/Draggable');
window.Arrive = require('arrive');
window.BackgroundVideo = require('background-video');
window.isMobile = require('ismobilejs');
window.Countdown = require('./vendor/jquery.countdown');
window.jsSocials = require('../bower_components/jssocials/dist/jssocials');
window.iFrameResizer = require('iframe-resizer');
window.Swiper = require('swiper');
window.plyr = require('plyr');

// modules
window.Header = require('./_modules/header');
window.SitePage = require('./_modules/sitePage');
window.HomePage = require('./_modules/homePage');
window.UnfairAdvantage = require('./_modules/unfairAdvantage');
window.PodiumPredictor = require('./_modules/podiumPredictor');
window.PredictorPanel = require('./_modules/predictorPanel');
window.PredictorResults = require('./_modules/predictorResults');

// utils
window.PreloadBg = require('./utils/preload-bg');
window.RaceCountDown = require('./utils/raceCountDown');
window.SocialSharing = require('./utils/socialSharing');
window.WindowScroll = require('./utils/window-scroll');
window.ParallaxController = require('./utils/parallax');


jQuery(document).ready(function($) {

    // main app
    var PurePitwallApp = (function() {

        // init window events
        window.windowScroll = new WindowScroll();


        // document ready
        $(document).ready(function(){

            // init header (nav)
            var header = $('header#header');
            if (header.length) {
                for (var i = 0; i < header.length; i++) {
                    new Header($(header[i]));
                }
            }

            // init sitePage
            var sitePage = $('.sitePage');
            if (sitePage.length) {
                for (var i = 0; i < sitePage.length; i++) {
                    new SitePage($(sitePage[i]));
                }
            }

            // init preload bgImages
            var bgImages = $('.js-bgImg');
            if (bgImages.length) {
                for (var i = 0; i < bgImages.length; i++) {
                    new PreloadBg($(bgImages[i]));
                }
            }

            // preload bgImages dynamically loaded
            $('.sitePage').arrive('.js-bgImg',function(){
                var img = $(this);
                new PreloadBg(img);
            });

            // auto scroll page when loading more content
            $('.elm-button').on('click',function(){
                var newPos = $(this).offset().top;
                TweenMax.to(window, 1, {scrollTo:{y:newPos,offsetY:90},ease:Strong.easeInOut,delay:0.5});
            });

            // init homePage
            var homePage = $('.homePage');
            if (homePage.length) {
                for (var i = 0; i < homePage.length; i++) {
                    new HomePage($(homePage[i]));
                }
            }

            // init unfairAdvantage
            var unfairAdvantage = $('.unfairAdvantage');
            if (unfairAdvantage.length) {
                for (var i = 0; i < unfairAdvantage.length; i++) {
                    new UnfairAdvantage($(unfairAdvantage[i]));
                }
            }

            // load races, and drivers, and init dependent modules after loaded
            window.$races = [];
            window.$drivers = [];
            window.siteUrl;
            var raceCountDown = $('.js-raceCountDown');
            var podiumPredictor = $('.podiumPredictor');
            var predictorPanel = $('.PP_predictorPanel');

            if (window.location.href.indexOf("local9") > -1){
                window.siteUrl = 'https://purepitwall.local9:3000'; // dev url
            } else if (window.location.href.indexOf("staging") > -1) {
                window.siteUrl = 'http://staging.purepitwall.com'; // staging
            } else {
                window.siteUrl = 'https://www.purepitwall.com'; // live
            }

            // load races
            $.getJSON(window.siteUrl+"/wp-content/themes/purepitwall/js/2018raceCalendar.json", function(data) {
                $.each(data.races,function(key,val) {
                    window.$races.push(val);
                });

            })
            .done(function() {
                window.$racesLoaded = true;
                // init raceCountDowns
                if (raceCountDown.length) {
                    for (var i = 0; i < raceCountDown.length; i++) {
                        new RaceCountDown($(raceCountDown[i]));
                    }
                }

                // load drivers
                $.getJSON(window.siteUrl+"/wp-content/themes/purepitwall/js/2018raceDrivers.json", function(data) {
                    $.each(data.drivers,function(key,val) {
                        window.$drivers.push(val);
                    });

                })
                .done(function() {
                    window.$driversLoaded = true;
                    // init podiumPredictor
                    if (podiumPredictor.length) {
                        for (var i = 0; i < podiumPredictor.length; i++) {
                            new PodiumPredictor($(podiumPredictor[i]));
                        }
                    }

                    // init predictorPanel
                    if (predictorPanel.length) {
                        for (var i = 0; i < predictorPanel.length; i++) {
                            new PredictorPanel($(predictorPanel[i]));
                        }
                    }

                })
                .fail(function() {

                })

            })
            .fail(function() {

            })

            // init socialSharing
            var socialSharing = $('.js-socialSharing');
            if (socialSharing.length) {
                for (var i = 0; i < socialSharing.length; i++) {
                    new SocialSharing($(socialSharing[i]));
                }
            }

            // init socialSharing on BlockQuotes
            var blockQuotes = $('blockquote');
            if (blockQuotes.length) {
                for (var i = 0; i < blockQuotes.length; i++) {
                    new SocialSharing($(blockQuotes[i]));
                }
            }

            // auto resize iframe on pitwall
            $('.pitwallContainer iframe').iFrameResize();


            // init parallax effects
            var parallaxModules = $('.is-parallax');
            if (parallaxModules.length) {
                for (var i = 0; i < parallaxModules.length; i++) {
                    new ParallaxController($(parallaxModules[i]));
                }
            }

            // trigger scroll events (window refresh fix)
            $(window).scrollTop($(window).scrollTop()+1);
            $(window).scrollTop($(window).scrollTop()-1);



            // init plyr
            var players = plyr.setup('.js-player');


            // init predictorResults
            var predictorResults = $('.predictorResults');
            if (predictorResults.length) {
                new PredictorResults(predictorResults);
            }


        });


        // page load
        $(window).on('load', function() {



        });

        // page resize
        $(window).on('resize', function() {
            window.w = window.innerWidth;
            window.h = window.innerHeight;

        });

    })();


});
