<?php get_header(); ?>
<section class="sitePage videoGallery" role="main">
    <div class="pageHero">
        <?php $id = get_queried_object_id(); if (has_post_thumbnail( $id) ): ?>
            <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'single-post-thumbnail' ); ?>
                <div class="pageHeroBg js-bgImg" style="background-image: url('<?php echo $image[0]; ?>')">
                </div>
        <?php endif; ?>
        <div class="pageHeroBgOverlay"></div>
        <div class="pageHeroContent">
            <div class="container-fluid">
                <div class="row no-gutter">
                    <h1><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
    </div>

    <div class="pageContent" style="background-color:black;background-image:url('<?php echo get_template_directory_uri();?>/assets/img/videogallery_background.jpg')">
        <div class="container-fluid">
            <div class="row no-gutter">
                <div class="">
                    <!-- pageBody -->
                    <h2>
                        WATCH IN-DEPTH VIDEO CONTENT
                    </h2>
                    <div class="subtitle">
                        Watch in detail how each Grand Prix unfolds with our post-race video content and exclusive interviews.
                    </div>

                    <div class="mainVideo">
                        <?php /* Start the Pods Loop */
                        // Pulling PODS Loop instead of WP Loop
                        $mypod = pods('ua_videos');
                        $params = array(
                            'limit' => 1,
                             'orderby' => 'date DESC',
                            );
                        $mypod->find($params);
                        ?>

                        <?php while ( $mypod->fetch() ) : ?>

                            <div class="left">
                                <?php
                                $title = $mypod->field('title');
                                $description = $mypod->field('video_description');
                                $video = $mypod->id();
                                $youtubeLink = $mypod->field('youtube_link');
                                $videoFile = wp_get_attachment_url($mypod->field('video.ID'));
                                $videoPoster = wp_get_attachment_url($mypod->field('video_poster.ID'));
                                $postDate = date_create_from_format('Y-m-d H:s', $mypod->display('post_date') );
                            ?>

                            <?php if ($videoFile) : ?>
                                <div class="videoWrapper">
                                    <video controls poster="<?php echo $videoPoster ?>" class="js-player">
                                        <source src="<?php echo $videoFile ?>" type="video/mp4">
                                    </video>
                                </div>
                            <?php endif; ?>

                            <?php if ($youtubeLink) : ?>
                                <div class="videoWrapper">
                                    <div data-type="youtube" data-video-id="<?php parse_str( parse_url( $youtubeLink, PHP_URL_QUERY ), $my_array_of_vars ); echo $my_array_of_vars['v']; ?>" class="js-player"></div>
                                </div>
                            <?php endif; ?>

                            </div>
                            <div class="right">
                                <h3><?php echo $title ?></h3>
                                <h5><?php echo date_format($postDate, 'd.m.Y'); ?></h5>
                                <p><?php echo $description ?></p>
                            </div>


                        <?php endwhile; ?>
                    </div>

                    <div class="allVideosContainer">
                        <?php /* Start the Pods Loop */
                            // Pulling PODS Loop instead of WP Loop
                            $mypod = pods('ua_videos');
                            $params = array(
                                 'orderby' => 'date DESC',
                                 'offset' => 1,
                                 'limit' => -1
                                );
                            $mypod->find($params);
                        ?>

                        <?php while ( $mypod->fetch() ) : ?>
                            <?php
                                $title = $mypod->field('title');
                                $description = $mypod->field('video_description');
                                $video = $mypod->id();
                                $youtubeLink = $mypod->field('youtube_link');
                                $videoFile = wp_get_attachment_url($mypod->field('video.ID'));
                                $videoPoster = wp_get_attachment_url($mypod->field('video_poster.ID'));
                                $postDate = date_create_from_format('Y-m-d H:s', $mypod->display('post_date') );
                            ?>

                            <div class="singleVideo">
                                <?php if ($videoFile) : ?>
                                    <div class="videoWrapper">
                                        <video controls poster="<?php echo $videoPoster ?>" class="js-player">
                                            <source src="<?php echo $videoFile ?>" type="video/mp4">
                                        </video>
                                    </div>
                                <?php endif; ?>

                                <?php if ($youtubeLink) : ?>
                                    <div class="videoWrapper">
                                        <div data-type="youtube" data-video-id="<?php parse_str( parse_url( $youtubeLink, PHP_URL_QUERY ), $my_array_of_vars ); echo $my_array_of_vars['v']; ?>" class="js-player"></div>
                                    </div>
                                <?php endif; ?>

                                <h4><?php echo $title ?></h4>
                                <h5><?php echo date_format($postDate, 'd.m.Y'); ?></h5>
                                <p><?php echo $description ?></p>
                            </div>
                        <?php endwhile; ?>

                    </div>

                    <!-- end of pageBody -->
                </div>
            </div>
        </div>

    </div>


</section>
<?php get_footer(); ?>
