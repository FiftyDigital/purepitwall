<?php get_header(); ?>
<section class="sitePage influencerInsight" role="main">
    <div class="pageHero">
        <?php $id = get_queried_object_id(); if (has_post_thumbnail( $id) ): ?>
            <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'single-post-thumbnail' ); ?>
                <div class="pageHeroBg js-bgImg" style="background-image: url('<?php echo $image[0]; ?>')">
                </div>
        <?php endif; ?>
        <div class="pageHeroBgOverlay"></div>
        <div class="pageHeroContent">
            <div class="container-fluid">
                <div class="row no-gutter">
                    <h1><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
    </div>

    <div class="pageContent">
        <div class="container-fluid">
            <div class="row no-gutter">
                <div class="pageBody col-xs-12 col-lg-8">
                    <!-- pageBody -->
                    <div class="row no-gutter">
                        <div class="intro col-xs-12 col-lg-9">
                            <h2>What you’re saying</h2>
                            <p>A hub of all the best live updates from the amazing fans of Pure Pit Wall and key Formula One influencers.</p>
                        </div>
                        <div class="col-lg-4">

                        </div>
                    </div>



                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row no-gutter">
                <?php echo do_shortcode('[ff id="1"]') ?>
            </div>
        </div>
    </div>


</section>
<?php get_footer(); ?>
