<div class="clear"></div>
</div>
<footer id="footer" role="contentinfo">

    <div class="container-fluid">
        <div class="row no-gutter">

            <?php if (!is_page('Pure Pit Wall')) : ?>
            <div class="col-xs-12 col-lg-3">
                <div class="titleHolder broughtBy">
                    Powered by
                </div>
                <div class="logoHolder">
                    <img src="<?php echo get_template_directory_uri();?>/assets/img/ps_mercedes.png">
                    <a href="http://www.purestorage.com/uk/" target="new">
                        <div class="hotspot ps"></div>
                    </a>
                    <a href="https://www.mercedesamgf1.com" target="new">
                        <div class="hotspot mercedes"></div>
                    </a>
                </div>
            </div>
            <?php endif ?>


            <div class="col-xs-12 col-lg-4 col-md-6 <?php if (!is_page('Pure Pit Wall')) : ?> col-lg-push-1 <?php endif ?>">
                <div class="titleHolder">
                    <h3>Sections</h3>
                </div>
                <div class="linksHolder sections">
                    <?php wp_nav_menu( array( 'menu' => 'footer-sections' ) ); ?>
                </div>

            </div>
            <div class="col-xs-12 col-lg-2 col-md-3 col-lg-push-1">
                <div class="titleHolder">
                    <h3>Find<span class="secondWord">Us</span></h3>
                </div>
                <div class="linksHolder social">
                    <ul>
                        <li><a href="https://www.facebook.com/PurePitWall" target="new"><span class="Icon Icon--facebook"></span> Facebook</a></li>
                        <li><a href="https://twitter.com/PurePitWall" target="new"><span class="Icon Icon--twitter"></span> Twitter</a></li>
                        <li><a href="https://www.linkedin.com/showcase/11059740/" target="new"><span class="Icon Icon--linkedin2"></span> LinkedIn</a></li>
                        <li><a href="http://www.purestorage.com" target="new"><span class="Icon Icon--purestorage"></span> Pure Storage</a></li>
                    </ul>
                </div>


            </div>
            <div class="col-xs-12 col-lg-2 col-md-3 col-lg-push-1">
                <div class="titleHolder">
                    <h3>Other<span class="secondWord">Links</span></h3>
                </div>
                <div class="linksHolder">
                    <?php wp_nav_menu( array( 'menu' => 'footer-other-links' ) ); ?>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
<?php wp_footer(); ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/main.min.js"></script>
<script id="__bs_script__">
    if (window.location.href.indexOf("localhost") > -1){
        //<![CDATA[
            document.write("<script async src='https://HOST:3000/browser-sync/browser-sync-client.js?v=2.18.8'><\/script>".replace("HOST", location.hostname));
        //]]>
    }
</script>
<script type="text/javascript">_satellite.pageBottom();</script>
</body>
</html>
