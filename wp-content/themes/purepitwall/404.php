<?php get_header(); ?>
<section id="content" role="main" class="sitePage">
    <div class="pageHero">
        <div class="pageHeroBgOverlay"></div>
        <div class="pageHeroContent">
            <div class="container-fluid">
                <div class="row no-gutter">
                    <h1>ooops!</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="pageContent">
        <div class="container-fluid">

            <div class="pageBody col-xs-12">
                <!-- pageBody -->
                    <article id="post-0" class="post not-found">
                        <header class="header">
                            <h1 class="entry-title"><?php _e( '404 Not Found', 'purepitwall' ); ?></h1>
                        </header>
                        <section class="entry-content">
                            <p><?php _e( 'We are very sorry, the page you are requesting does not exist.', 'purepitwall' ); ?></p>
                        </section>
                    </article>
                <!-- end of pageBody -->
            </div>
            <!-- pageSideBar -->
            <!-- <div class="pageSideBar col-xs-12 col-md-4">






            </div> -->
            <!-- end of pageSideBar -->
        </div>

    </div>

</section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
