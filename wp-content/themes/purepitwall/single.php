<?php get_header(); ?>
<section class="sitePage raceNotes raceNotesArticle" role="main">

    <div class="pageHero">
        <!-- <?php if (has_post_thumbnail( $post->ID ) ): ?>
            <?php
            $raceNotesPageID = wt_get_ID_by_page_name('racenotes');
            $image = wp_get_attachment_image_src( get_post_thumbnail_id($raceNotesPageID), 'single-post-thumbnail' ); ?>
                <div class="pageHeroBg js-bgImg" style="background-image: url('<?php echo $image[0]; ?>')">
                </div>
        <?php endif; ?> -->
        <div class="pageHeroBgOverlay"></div>
        <div class="pageHeroContent">
            <div class="container-fluid">
                <div class="row no-gutter">
                    <h1><?php echo get_the_title($raceNotesPageID); ?></h1>
                </div>
            </div>
        </div>
    </div>


    <div class="pageContent">
        <div class="container-fluid">
            <div class="row no-gutter">
                <div class="pageBody col-xs-12 col-lg-8">
                    <!-- pageBody -->
                    <div class="backLink">
                        <a href="./racenotes">
                            <div class="backIcon"></div>
                            <span class="backText">BACK TO <span class="bold">RACE</span>NOTES</span>
                        </a>
                    </div>
                    <div class="row no-gutter">
                        <div class="articlesContainer">

                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                            <?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>

                            <div class="articleBody">

                                <h2 class="articleTitle"><?php the_title(); ?></h2>

                                <h3 class="articleDate"><?php the_time( get_option( 'date_format' ) ); ?></h3>

                                <div class="js-socialSharing"></div>

                                <?php the_content(); ?>

                                <div class="js-socialSharing"></div>

                            </div>


                            <!-- <?php echo do_shortcode('[Sassy_Social_Share]') ?> -->

                        <?php endwhile; endif; ?>

                    </div>
                    <div class="backLink bottomLink">
                        <a href="./racenotes">
                            <div class="backIcon"></div>
                            <span class="backText">BACK TO <span class="bold">RACE</span>NOTES</span>
                        </a>
                    </div>
                    <!-- end of pageBody -->
                </div>

            </div>
            <div class="pageSideBar col-xs-12 col-lg-4">
                <!-- pageSideBar -->
                <div class="sideBarContent">
                    <div class="relatedBlogs">
                        <h3>You might also like:</h3>

                        <?php
                            $args = array('post_type' => 'post','orderby'   => 'rand','posts_per_page' => 4,'post__not_in' => array( $post->ID ));
                            $custom_query = new WP_Query($args);
                            while($custom_query->have_posts()) : $custom_query->the_post(); ?>

                            <div class="postArticle">
                                <a href="<?php the_permalink(); ?>" title="Read more">
                                    <div class="postArticleImage">
                                        <?php if (has_post_thumbnail( $post->ID ) ): ?>
                                          <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                                          <div class="imageWrapper">
                                              <div class="imageContainer js-bgImg" style="background-image: url('<?php echo $image[0]; ?>')">
                                              </div>
                                          </div>
                                        <?php endif; ?>
                                    </div>
                                    <div class="postArticleTitle">
                                        <h2><?php the_title(); ?></h2>
                                        <div class="postDate"><?php echo get_the_date( 'd.m.Y' ); ?></div>
                                    </div>
                                </a>
                            </div>

                        <?php endwhile; ?>
                        <?php wp_reset_postdata();?>

                    </div>




                    <!-- <?php get_sidebar(); ?> -->
                </div>



                <!-- end of pageSideBar -->
            </div>
        </div>

    </div>

</section>
<?php get_footer(); ?>
