<?php get_header(); ?>
<section class="sitePage podiumPredictor predictorResults <?php if (is_user_logged_in() ) {echo 'logged-in'; }; ?>" role="main">

    <div class="heading">
        <h1>2017 <span class="slim secondWord">WINNERS</span></h1>
    </div>

    <div class="resultsPodium">
        <div class="bgOverlay"></div>
        <div class="content">
            <h2>CONGRATULATIONS!</h2>
            <div class="podium">
                <div class="podiumPlace">
                    <div class="info">
                        <h3 class="winner">davidizki</h3>
                        <div class="prizeCircle" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/predictor_prize2.jpg')">
                        </div>
                        <div class="prizeCopy">
                            <h5>An Exclusive</h5>
                            <h3>Factory Tour</h3>
                        </div>
                    </div>
                    <div class="place place2">
                        <div class="placeLegend">2</div>
                    </div>
                </div>

                <div class="podiumPlace">
                    <div class="info">
                        <h3 class="winner">Hammertime</h3>
                        <div class="prizeCircle" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/predictor_prize1.jpg')">
                        </div>
                        <div class="prizeCopy">
                            <h6>Vip hospitality for two at the</h6>
                            <h2>MONACO</h2>
                            <h4>GRAND PRIX 2018</h4>
                        </div>
                    </div>
                    <div class="place place1">
                        <div class="placeLegend">1</div>
                    </div>
                </div>

                <div class="podiumPlace">
                    <div class="info">
                        <h3 class="winner">NandoRamos</h3>
                        <div class="prizeCircle" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/predictor_prize3.jpg')">
                        </div>
                        <div class="prizeCopy">
                            <h3>A Signed Cap</h3>
                            <h6>By both Mercedes-AMG Petronas Motorsport Drivers</h6>
                        </div>
                    </div>
                    <div class="place place3">
                        <div class="placeLegend">3</div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="finishStats">
        <div class="container-fluid">
            <h2>PODIUM FINISHING STATS</h2>
            <div class="statsHolder">
                <div class="statGroup left">
                    <div class="statPlace first">
                        <div class="winnerAvatar" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/predictor/winners/HAMMERTIME.jpg')"></div>
                        <h3 class="winner">Hammertime</h3>
                        <div class="statBox">
                            <div class="statLine">
                                <div class="statIcon">
                                    <img src="<?php echo get_template_directory_uri();?>/assets/img/statIcons_flags.svg" class="flags">
                                </div>
                                <div class="statCopy">
                                    <div class="statTitle">
                                        Total Score
                                    </div>
                                    <div class="statResult">
                                        1147
                                    </div>
                                </div>
                            </div>
                            <div class="statLine">
                                <div class="statIcon">
                                    <img src="<?php echo get_template_directory_uri();?>/assets/img/statIcons_trophy.svg">
                                </div>
                                <div class="statCopy">
                                    <div class="statTitle">
                                        Best Result
                                    </div>
                                    <div class="statResult">
                                        93 (AUS)
                                    </div>
                                </div>
                            </div>
                            <div class="statLine">
                                <div class="statIcon">
                                    <img src="<?php echo get_template_directory_uri();?>/assets/img/statIcons_timer.svg">
                                </div>
                                <div class="statCopy">
                                    <div class="statTitle">
                                        Closest Pole Time Prediction
                                    </div>
                                    <div class="statResult">
                                        Canada
                                    </div>
                                </div>
                            </div>
                            <div class="statLine">
                                <div class="statIcon">
                                    <img src="<?php echo get_template_directory_uri();?>/assets/img/statIcons_helmet.svg">
                                </div>
                                <div class="statCopy">
                                    <div class="statTitle">
                                        Best Podium Prediction
                                    </div>
                                    <div class="statResult">
                                        Australia
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="statPlace second">
                        <div class="winnerAvatar" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/predictor/winners/DAVIDIZKI.jpg')"></div>
                        <h3 class="winner">davidizki</h3>
                        <div class="statBox">
                            <div class="statLine">
                                <div class="statIcon">
                                    <img src="<?php echo get_template_directory_uri();?>/assets/img/statIcons_flags.svg" class="flags">
                                </div>
                                <div class="statCopy">
                                    <div class="statTitle">
                                        Total Score
                                    </div>
                                    <div class="statResult">
                                        1071
                                    </div>
                                </div>
                            </div>
                            <div class="statLine">
                                <div class="statIcon">
                                    <img src="<?php echo get_template_directory_uri();?>/assets/img/statIcons_trophy.svg">
                                </div>
                                <div class="statCopy">
                                    <div class="statTitle">
                                        Best Result
                                    </div>
                                    <div class="statResult">
                                        103 (ITA)
                                    </div>
                                </div>
                            </div>
                            <div class="statLine">
                                <div class="statIcon">
                                    <img src="<?php echo get_template_directory_uri();?>/assets/img/statIcons_timer.svg">
                                </div>
                                <div class="statCopy">
                                    <div class="statTitle">
                                        Closest Pole Time Prediction
                                    </div>
                                    <div class="statResult">
                                        China
                                    </div>
                                </div>
                            </div>
                            <div class="statLine">
                                <div class="statIcon">
                                    <img src="<?php echo get_template_directory_uri();?>/assets/img/statIcons_helmet.svg">
                                </div>
                                <div class="statCopy">
                                    <div class="statTitle">
                                        Best Podium Prediction
                                    </div>
                                    <div class="statResult">
                                        Italy
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="statGroup right">
                    <div class="statPlace third">
                        <div class="winnerAvatar" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/predictor/winners/NANDORAMOS.jpg')"></div>
                        <h3 class="winner">NandoRamos</h3>
                        <div class="statBox last">
                            <div class="statLine">
                                <div class="statIcon">
                                    <img src="<?php echo get_template_directory_uri();?>/assets/img/statIcons_flags.svg" class="flags">
                                </div>
                                <div class="statCopy">
                                    <div class="statTitle">
                                        Total Score
                                    </div>
                                    <div class="statResult">
                                        1031
                                    </div>
                                </div>
                            </div>
                            <div class="statLine">
                                <div class="statIcon">
                                    <img src="<?php echo get_template_directory_uri();?>/assets/img/statIcons_trophy.svg">
                                </div>
                                <div class="statCopy">
                                    <div class="statTitle">
                                        Best Result
                                    </div>
                                    <div class="statResult">
                                        103 (HUN)
                                    </div>
                                </div>
                            </div>
                            <div class="statLine">
                                <div class="statIcon">
                                    <img src="<?php echo get_template_directory_uri();?>/assets/img/statIcons_timer.svg">
                                </div>
                                <div class="statCopy">
                                    <div class="statTitle">
                                        Closest Pole Time Prediction
                                    </div>
                                    <div class="statResult">
                                        China
                                    </div>
                                </div>
                            </div>
                            <div class="statLine">
                                <div class="statIcon">
                                    <img src="<?php echo get_template_directory_uri();?>/assets/img/statIcons_helmet.svg">
                                </div>
                                <div class="statCopy">
                                    <div class="statTitle">
                                        Best Podium Prediction
                                    </div>
                                    <div class="statResult">
                                        Bahrain
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="raceByRace">
        <div class="holder">
            <h2>RACE-BY-RACE</h2>

            <div class="resultsSlider">
                <div class="swiper-container">
                    <div class="swiper-wrapper">

                    </div>
                </div>
                <div class="controls">
                    <div class="prevArrow"><div class="arrow left"></div></div>
                    <div class="nextArrow"><div class="arrow right"></div></div>
                </div>
            </div>

            <div class="flagsSlider">
                <div class="swiper-container">
                    <div class="swiper-wrapper">

                    </div>
                </div>
            </div>



        </div>
    </div>

    <div class="leaderboard">
        <div class="holder">
            <h1>2017 <span class="slim secondWord">FINAL STANDINGS</span></h1>

            <div class="leaderBoardHolder">

                <div class="mainLeaderBoardPanel">
                    <div class="row leaderBoardSelect">
                        <h3>Leaderboard</h3>
                        <div class="raceDisplay leadBD">
                            <div class="raceSelect">
                                <div class="currentRace">
                                    <div class="optionFlag"></div>
                                    <div class="currentRaceName"></div>
                                </div>
                                <div class="arrow">
                                    <div class="Icon Icon--angle-down"></div>
                                </div>
                                <div class="raceDropDownHolder">
                                    <div class="raceDropDown"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panelContent statsContainer">
                        <div class="statsHolder Overall selected">
                            <?php echo do_shortcode('[motorracingleague results=1 used_doubleups=0]') ?>
                        </div>

                        <div class="statsHolder AustralianGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=1]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=1]') ?>
                            </div>
                        </div>

                        <div class="statsHolder ChineseGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=2]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=2]') ?>
                            </div>
                        </div>

                        <div class="statsHolder BahrainGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=3]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=3]') ?>
                            </div>
                        </div>

                        <div class="statsHolder RussianGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=4]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=4]') ?>
                            </div>
                        </div>

                        <div class="statsHolder SpanishGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=5]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=5]') ?>
                            </div>
                        </div>

                        <div class="statsHolder MonacoGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=6]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=6]') ?>
                            </div>
                        </div>

                        <div class="statsHolder CanadianGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=7]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=7]') ?>
                            </div>
                        </div>

                        <div class="statsHolder AzerbaijanGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=8]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=8]') ?>
                            </div>
                        </div>

                        <div class="statsHolder AustrianGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=9]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=9]') ?>
                            </div>
                        </div>

                        <div class="statsHolder BritishGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=10]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=10]') ?>
                            </div>
                        </div>

                        <div class="statsHolder HungarianGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=11]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=11]') ?>
                            </div>
                        </div>

                        <div class="statsHolder BelgianGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=12]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=12]') ?>
                            </div>
                        </div>

                        <div class="statsHolder ItalianGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=13]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=13]') ?>
                            </div>
                        </div>

                        <div class="statsHolder SingaporeGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=14]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=14]') ?>
                            </div>
                        </div>

                        <div class="statsHolder MalaysianGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=15]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=15]') ?>
                            </div>
                        </div>

                        <div class="statsHolder JapaneseGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=16]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=16]') ?>
                            </div>
                        </div>

                        <div class="statsHolder USAGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=17]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=17]') ?>
                            </div>
                        </div>

                        <div class="statsHolder MexicanGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=18]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=18]') ?>
                            </div>
                        </div>

                        <div class="statsHolder BrazilGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=19]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=19]') ?>
                            </div>
                        </div>

                        <div class="statsHolder AbuDhabiGP">
                            <div class="preDeadline">
                                <?php echo do_shortcode('[motorracingleague full=0 race=20]') ?>
                            </div>
                            <div class="postDeadline">
                                <?php echo do_shortcode('[motorracingleague full=1 race=20]') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="nextSeason">
        <div class="swiper-container">
            <div class="swiper-wrapper">

                <div class="swiper-slide" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/000_PPW_Carousel_.jpg')">
                    <h1>2018 PODIUM<br>PREDICTOR GAME</h1>
                </div>
                <div class="swiper-slide" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/001_PPW_Carousel_.jpg')">
                    <h1>BIGGER PRIZES</h1>
                </div>
                <div class="swiper-slide" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/002_PPW_Carousel_.jpg')">
                    <h1>A NEW FORMAT</h1>
                </div>
                <div class="swiper-slide" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/003_PPW_Carousel_.jpg')">
                    <h1>MORE WAYS TO WIN</h1>
                </div>

            </div>
        </div>
        <div class="pagination"></div>

    </div>



</section>
<?php get_footer(); ?>
