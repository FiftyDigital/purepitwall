<?php
add_action( 'after_setup_theme', 'purepitwall_setup' );
function purepitwall_setup()
{
load_theme_textdomain( 'purepitwall', get_template_directory() . '/languages' );
add_theme_support( 'title-tag' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails' );
global $content_width;
if ( ! isset( $content_width ) ) $content_width = 640;
register_nav_menus(
array( 'main-menu' => __( 'Main Menu', 'purepitwall' ) )
);
}
add_action( 'wp_enqueue_scripts', 'purepitwall_load_scripts' );
function purepitwall_load_scripts()
{
wp_enqueue_script( 'jquery' );
}
add_action( 'comment_form_before', 'purepitwall_enqueue_comment_reply_script' );
function purepitwall_enqueue_comment_reply_script()
{
if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}
add_filter( 'the_title', 'purepitwall_title' );
function purepitwall_title( $title ) {
if ( $title == '' ) {
return '&rarr;';
} else {
return $title;
}
}
add_filter( 'wp_title', 'purepitwall_filter_wp_title' );
function purepitwall_filter_wp_title( $title )
{
return $title . esc_attr( get_bloginfo( 'name' ) );
}
add_action( 'widgets_init', 'purepitwall_widgets_init' );
function purepitwall_widgets_init()
{
register_sidebar( array (
'name' => __( 'Sidebar Widget Area', 'purepitwall' ),
'id' => 'primary-widget-area',
'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
'after_widget' => "</li>",
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );
}
function purepitwall_custom_pings( $comment )
{
$GLOBALS['comment'] = $comment;
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
<?php
}
add_filter( 'get_comments_number', 'purepitwall_comments_number' );
function purepitwall_comments_number( $count )
{
if ( !is_admin() ) {
global $id;
$comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
return count( $comments_by_type['comment'] );
} else {
return $count;
}
}
function wt_get_ID_by_page_name($page_name)
{
    global $wpdb;
    $page_name_id = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name = '".$page_name."'");
    return $page_name_id;
}
add_action( 'wp_login_failed', 'custom_login_failed' );
function custom_login_failed( $username )
{
    $referrer = wp_get_referer();

    if ( $referrer && ! strstr($referrer, 'wp-login') && ! strstr($referrer,'wp-admin') )
    {
        wp_redirect( add_query_arg('login', 'failed', $referrer) );
        exit;
    }
}
add_filter( 'authenticate', 'custom_authenticate_username_password', 30, 3);
function custom_authenticate_username_password( $user, $username, $password )
{
    if ( is_a($user, 'WP_User') ) { return $user; }

    if ( empty($username) || empty($password) )
    {
        $error = new WP_Error();
        $user  = new WP_Error('authentication_failed', __('<strong>ERROR</strong>: Invalid username or incorrect password.'));

        return $error;
    }
}
/**
 * @um_user_after_updating_profile
 */
function doggycom_redirect_after_updating_profile( $to_update ){

    if ( is_user_logged_in() ) {

        exit( wp_redirect( remove_query_arg( array('profiletab','um_action') ) ) );

    }
}
add_action('um_user_after_updating_profile','doggycom_redirect_after_updating_profile',10,1);


add_theme_support('post-thumbnails');

function switch_to_relative_url($html, $id, $caption, $title, $align, $url, $size, $alt)
{
$imageurl = wp_get_attachment_image_src($id, $size);
$relativeurl = wp_make_link_relative($imageurl[0]);
$html = str_replace($imageurl[0],$relativeurl,$html);

return $html;
}
add_filter('image_send_to_editor','switch_to_relative_url',10,8);
